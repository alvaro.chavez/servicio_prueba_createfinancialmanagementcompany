package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.util.Enums;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class CreateFinancialManagementCompanyMapperTest {

    @InjectMocks
    private CreateFinancialManagementCompanyMapper mapper;
    @Mock
    private Translator translator;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private void mapInEnumMapper() {
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_DOCUMENTTYPE_ID, "RUC")).thenReturn("R");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_BUSINESSMANAGEMENT_MANAGEMENTTYPE_ID,"JOINT")).thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_PRODUCT_ID,"NETCASH_BUSINESS")).thenReturn("E");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_VERSION_ID,"BASIC")).thenReturn("B");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_VERSION_ID,"ADVANCED")).thenReturn("A");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_PRODUCTTYPE_ID,"ACCOUNTS")).thenReturn("A");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_RELATEDCONTRACTS_RELATIONTYPE,"PAYING_ACCOUNT")).thenReturn("C");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_CONTACTDETAILS_CONTACTTYPE_ID,"EMAIL")).thenReturn("MA");
        when(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_REVIEWER_ID,"EXECUTIVE")).thenReturn("E");
    }

    @Test
    public void mapInFullTest() throws IOException {
        FinancialManagementCompanies input = EntityStubs.getInstance().getFinancialManagementCompanies(); // obtengo una instancia del canonico
        mapInEnumMapper();
        DTOIntFinancialManagementCompanies result = mapper.mapIn(input);  // transformo el canonico en un DTO
        assertNotNull(result);
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertNotNull(result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertNotNull(result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertNotNull(result.getBusiness().getLimitAmount().getAmount());
        assertNotNull(result.getBusiness().getLimitAmount().getCurrency());
        assertNotNull(result.getNetcashType().getId());
        assertNotNull(result.getNetcashType().getVersion().getId());
        assertNotNull(result.getContract().getId());
        assertNotNull(result.getProduct().getId());
        assertNotNull(result.getProduct().getProductType().getId());
        assertNotNull(result.getRelationType().getId());
        assertNotNull(result.getReviewers().get(0).getUnitManagement());
        assertNotNull(result.getReviewers().get(0).getRegistrationIdentifier());
        assertNotNull(result.getReviewers().get(0).getProfessionPosition());
        assertNotNull(result.getReviewers().get(0).getBusinessAgentId());
        assertNotNull(result.getReviewers().get(0).getBank().getId());
        assertNotNull(result.getReviewers().get(0).getBank().getBranch().getId());
        assertNotNull(result.getReviewers().get(0).getReviewerType().getId());
        assertNotNull(result.getReviewers().get(0).getProfile().getId());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertNotNull(result.getReviewers().get(0).getContactDetails().get(0).getContactType());

        //COMPARAR EL INPUT CON EL DTO OBTENIDO DEL MAPIN
        assertEquals("R", result.getBusiness().getBusinessDocuments().get(0).getBusinessDocumentType().getId());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getDocumentNumber(), result.getBusiness().getBusinessDocuments().get(0).getDocumentNumber());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getIssueDate(), result.getBusiness().getBusinessDocuments().get(0).getIssueDate());
        assertEquals(input.getBusiness().getBusinessDocuments().get(0).getExpirationDate(), result.getBusiness().getBusinessDocuments().get(0).getExpirationDate());
        assertEquals("C", result.getBusiness().getBusinessManagement().getManagementType().getId());
        assertEquals(input.getBusiness().getLimitAmount().getAmount(), result.getBusiness().getLimitAmount().getAmount());
        assertEquals(input.getBusiness().getLimitAmount().getCurrency(), result.getBusiness().getLimitAmount().getCurrency());
        assertEquals("E", result.getNetcashType().getId());
        assertEquals("A", result.getNetcashType().getVersion().getId());
        assertEquals("111", result.getContract().getId());
        assertEquals("123", result.getProduct().getId());
        assertEquals("A", result.getProduct().getProductType().getId());
        assertEquals("C", result.getRelationType().getId());
        assertEquals(input.getReviewers().get(0).getBusinessAgentId(), result.getReviewers().get(0).getBusinessAgentId());
        assertEquals(input.getReviewers().get(0).getContactDetails().get(0).getContact(), result.getReviewers().get(0).getContactDetails().get(0).getContact());
        assertEquals("MA", result.getReviewers().get(0).getContactDetails().get(0).getContactType());
        assertEquals("E", result.getReviewers().get(0).getReviewerType().getId());
        assertEquals(input.getReviewers().get(0).getUnitManagement(), result.getReviewers().get(0).getUnitManagement());
        assertEquals(input.getReviewers().get(0).getBank().getId(), result.getReviewers().get(0).getBank().getId());
        assertEquals(input.getReviewers().get(0).getBank().getBranch().getId(), result.getReviewers().get(0).getBank().getBranch().getId());
        assertEquals(input.getReviewers().get(0).getProfile().getId(), result.getReviewers().get(0).getProfile().getId());
        assertEquals(input.getReviewers().get(0).getProfessionPosition(), result.getReviewers().get(0).getProfessionPosition());
        assertEquals(input.getReviewers().get(0).getRegistrationIdentifier(), result.getReviewers().get(0).getRegistrationIdentifier());

    }

    @Test
    public void mapInEmptyTest() {
        DTOIntFinancialManagementCompanies result=mapper.mapIn(new FinancialManagementCompanies());
        assertNotNull(result);
        assertNull(result.getBusiness());
        assertNull(result.getProduct());
        assertNull(result.getContract());
        assertNull(result.getRelationType());
        assertNull(result.getNetcashType());
        assertNull(result.getReviewers());

    }
    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<FinancialManagementCompanies> result = mapper.mapOut(EntityStubs.getInstance().getFinancialManagementCompanies());
        assertNotNull(result);
        assertNotNull(result.getData());
    }
    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<FinancialManagementCompanies> result = mapper.mapOut(new FinancialManagementCompanies());
        assertNotNull(result);
        assertNotNull(result.getData());
    }


}
