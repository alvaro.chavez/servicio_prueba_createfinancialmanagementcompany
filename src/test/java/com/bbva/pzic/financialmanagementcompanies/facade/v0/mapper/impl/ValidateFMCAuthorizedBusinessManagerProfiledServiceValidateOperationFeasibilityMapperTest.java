package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapperTest {

    private IValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper mapper;

    @Before
    public void setUp() {
        mapper = new ValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {
        ValidateOperationFeasibility input = EntityStubs.getInstance().getValidateOperationFeasibility();
        InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility result = mapper.mapIn(
                EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID,
                EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID,
                EntityStubs.PROFILED_SERVICE_ID,
                input);

        assertNotNull(result);
        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getAuthorizedBusinessManagerId());
        assertNotNull(result.getProfiledServiceId());
        assertNotNull(result.getValidateOperationFeasibility().getContract().getId());
        assertNotNull(result.getValidateOperationFeasibility().getOperationAmount().getAmount());
        assertNotNull(result.getValidateOperationFeasibility().getOperationAmount().getCurrency());

        assertEquals(EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID, result.getFinancialManagementCompanyId());
        assertEquals(EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID, result.getAuthorizedBusinessManagerId());
        assertEquals(EntityStubs.PROFILED_SERVICE_ID, result.getProfiledServiceId());
        assertEquals(input.getContract().getId(), result.getValidateOperationFeasibility().getContract().getId());
        assertEquals(input.getOperationAmount().getAmount(), result.getValidateOperationFeasibility().getOperationAmount().getAmount());
        assertEquals(input.getOperationAmount().getCurrency(), result.getValidateOperationFeasibility().getOperationAmount().getCurrency());
    }

    @Test
    public void mapInEmptyTest() {
        InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility result = mapper.mapIn(
                EntityStubs.FINANCIAL_MANAGEMENT_COMPANY_ID,
                EntityStubs.AUTHORIZED_BUSINESS_MANAGER_ID,
                EntityStubs.PROFILED_SERVICE_ID,
                new ValidateOperationFeasibility());

        assertNotNull(result.getFinancialManagementCompanyId());
        assertNotNull(result.getAuthorizedBusinessManagerId());
        assertNotNull(result.getProfiledServiceId());
        assertNull(result.getValidateOperationFeasibility());
    }

    @Test
    public void mapOutFullTest() {
        ServiceResponse<ValidateOperationFeasibility> result = mapper.mapOut(new ValidateOperationFeasibility());
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() {
        ServiceResponse<ValidateOperationFeasibility> result = mapper.mapOut(null);
        assertNull(result);
    }
}
