package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.mock.FormatsKwlvMock;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ValidateOperationFeasibility;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMappTest {

    @InjectMocks
    private TxValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() throws IOException {
        InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility input = EntityStubs.getInstance().getInputValidateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility();
        FormatoKNECLVE0 result = mapper.mapIn(input);

        assertNotNull(result.getCodemp());
        assertNotNull(result.getUsuid());
        assertNotNull(result.getCodserv());
        assertNotNull(result.getCtacarg());
        assertNotNull(result.getImpoper());
        assertNotNull(result.getDiviope());

        assertEquals(input.getFinancialManagementCompanyId(), result.getCodemp());
        assertEquals(input.getAuthorizedBusinessManagerId(), result.getUsuid());
        assertEquals(input.getProfiledServiceId(), result.getCodserv());
        assertEquals(input.getValidateOperationFeasibility().getContract().getId(), result.getCtacarg());
        assertEquals(input.getValidateOperationFeasibility().getOperationAmount().getAmount(), result.getImpoper());
        assertEquals(input.getValidateOperationFeasibility().getOperationAmount().getCurrency(), result.getDiviope());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKNECLVE0 result = mapper.mapIn(new InputValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility());

        assertNull(result.getCodemp());
        assertNull(result.getUsuid());
        assertNull(result.getCodserv());
        assertNull(result.getCtacarg());
        assertNull(result.getImpoper());
        assertNull(result.getDiviope());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoKNECLVS0 format = FormatsKwlvMock.getInstance().getFormatoKNECLVS0();

        when(translator.translateBackendEnumValueStrictly(
                "profiledServices.validateOperationFeasibility.result.id", format.getIndlimi()))
                .thenReturn("INVALID");

        ValidateOperationFeasibility result = mapper.mapOut(format);

        assertNotNull(result.getResult().getId());
        assertNotNull(result.getResult().getReason());

        assertEquals("INVALID", result.getResult().getId());
        assertEquals(format.getDesclim(), result.getResult().getReason());
    }

    @Test
    public void mapOutEmptyTest() {
        ValidateOperationFeasibility result = mapper.mapOut(new FormatoKNECLVS0());

        assertNull(result.getResult());
    }
}
