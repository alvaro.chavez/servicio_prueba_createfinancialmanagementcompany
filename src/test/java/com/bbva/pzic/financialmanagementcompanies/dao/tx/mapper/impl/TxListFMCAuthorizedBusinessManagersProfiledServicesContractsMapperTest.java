package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.EntityStubs;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntServiceContract;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListFMCAuthorizedBusinessManagersProfiledServicesContracts;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.FormatoKNECLCS1;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlc.mock.FormatsKwlcMock;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
@RunWith(MockitoJUnitRunner.class)
public class TxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapperTest {

    @InjectMocks
    private TxListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper mapper;
    @Mock
    private Translator translator;

    @Test
    public void mapInFullTest() {
        InputListFMCAuthorizedBusinessManagersProfiledServicesContracts input = EntityStubs.getInstance().getInputListFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts();
        FormatoKNECLCE0 result = mapper.mapIn(input);

        assertNotNull(result.getCodcli());
        assertNotNull(result.getCodusu());
        assertNotNull(result.getCodser());
        assertNotNull(result.getIdpagin());
        assertNotNull(result.getTampagi());

        assertEquals(input.getFinancialManagementCompanyId(), result.getCodcli());
        assertEquals(input.getAuthorizedBusinessManagerId(), result.getCodusu());
        assertEquals(input.getProfiledServiceId(), result.getCodser());
        assertEquals(input.getPaginationKey(), result.getIdpagin());
        assertEquals(input.getPageSize(), result.getTampagi());
    }

    @Test
    public void mapInEmptyTest() {
        FormatoKNECLCE0 result = mapper.mapIn(new InputListFMCAuthorizedBusinessManagersProfiledServicesContracts());

        assertNull(result.getCodcli());
        assertNull(result.getCodusu());
        assertNull(result.getCodser());
        assertNull(result.getIdpagin());
        assertNull(result.getTampagi());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        FormatoKNECLCS0 format = FormatsKwlcMock.getInstance().getFormatoKNECLCS0s().get(0);

        when(translator.translateFrontendEnumValueStrictly(
                "profiledServices.contracts.contractType", format.getTipope()))
                .thenReturn("PAYER");
        when(translator.translateFrontendEnumValueStrictly(
                "profiledServices.contracts.NumberType", format.getTipcon()))
                .thenReturn("CCC");
        when(translator.translateFrontendEnumValueStrictly(
                "authorizedBusinessManager.operationsRights.permissionType.id", format.getTipper()))
                .thenReturn("QUERY_AND_SIGNATURE");
        when(translator.translateFrontendEnumValueStrictly(
                "authorizedBusinessManager.signature.id", format.getPodval()))
                .thenReturn("JOINT_SIGNATURE_OF_SEVERAL");
        when(translator.translateFrontendEnumValueStrictly(
                "profiledServices.contracts.product.productType", format.getIdepro()))
                .thenReturn("FUNDS");

        DTOIntServiceContract result = new DTOIntServiceContract();
        result = mapper.mapOut(format, result);
        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(1, result.getData().size());
        assertNotNull(result.getData().get(0));
        assertNotNull(result.getData().get(0).getId());
        assertNotNull(result.getData().get(0).getContractType());
        assertNotNull(result.getData().get(0).getNumber());
        assertNotNull(result.getData().get(0).getNumberType());
        assertNotNull(result.getData().get(0).getIsFavourite());
        assertNotNull(result.getData().get(0).getAlias());
        assertNotNull(result.getData().get(0).getCurrency());
        assertNotNull(result.getData().get(0).getCountry());
        assertNotNull(result.getData().get(0).getCountry().getId());
        assertNotNull(result.getData().get(0).getBank());
        assertNotNull(result.getData().get(0).getBank().getId());
        assertNotNull(result.getData().get(0).getBank().getName());
        assertNotNull(result.getData().get(0).getBank().getBranch());
        assertNotNull(result.getData().get(0).getBank().getBranch().getId());
        assertNotNull(result.getData().get(0).getBank().getBranch().getName());
        assertNotNull(result.getData().get(0).getInternalContractNumber());
        assertNotNull(result.getData().get(0).getCounterparty());
        assertNotNull(result.getData().get(0).getOperationRights());
        assertNotNull(result.getData().get(0).getOperationRights().getPermissionType());
        assertNotNull(result.getData().get(0).getOperationRights().getPermissionType().getId());
        assertNotNull(result.getData().get(0).getOperationRights().getPermissionType().getName());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature().getValidationRights());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature().getValidationRights().getId());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature().getValidationRights().getName());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature().getAmountLimit());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature().getAmountLimit().getAmount());
        assertNotNull(result.getData().get(0).getOperationRights().getSignature().getAmountLimit().getCurrency());
        assertNotNull(result.getData().get(0).getHolder());
        assertNotNull(result.getData().get(0).getHolder().getFullName());
        assertNotNull(result.getData().get(0).getBalance());
        assertNotNull(result.getData().get(0).getBalance().getTotalAmount());
        assertNotNull(result.getData().get(0).getBalance().getTotalAmount().getAmount());
        assertNotNull(result.getData().get(0).getBalance().getTotalAmount().getCurrency());
        assertNotNull(result.getData().get(0).getProduct());
        assertNotNull(result.getData().get(0).getProduct().getId());
        assertNotNull(result.getData().get(0).getProduct().getName());
        assertNotNull(result.getData().get(0).getProduct().getProductType());
        assertNotNull(result.getData().get(0).getProduct().getProductType().getId());
        assertNotNull(result.getData().get(0).getProduct().getProductType().getDescription());

        assertEquals(format.getNumcon(), result.getData().get(0).getId());
        assertEquals("PAYER", result.getData().get(0).getContractType());
        assertEquals(format.getNumcon(), result.getData().get(0).getNumber());
        assertEquals(format.getTipcon(), result.getData().get(0).getNumberType());
        assertTrue(result.getData().get(0).getIsFavourite());
        assertEquals(format.getAlias(), result.getData().get(0).getAlias());
        assertEquals(format.getDivcon(), result.getData().get(0).getCurrency());
        assertEquals(format.getPais(), result.getData().get(0).getCountry().getId());
        assertEquals(format.getIdeban(), result.getData().get(0).getBank().getId());
        assertEquals(format.getNomban(), result.getData().get(0).getBank().getName());
        assertEquals(format.getIdeofi(), result.getData().get(0).getBank().getBranch().getId());
        assertEquals(format.getNomofi(), result.getData().get(0).getBank().getBranch().getName());
        assertEquals(format.getNumint(), result.getData().get(0).getInternalContractNumber());
        assertEquals(format.getNumext(), result.getData().get(0).getCounterparty());
        assertEquals("QUERY_AND_SIGNATURE", result.getData().get(0).getOperationRights().getPermissionType().getId());
        assertEquals(format.getDestip(), result.getData().get(0).getOperationRights().getPermissionType().getName());
        assertEquals("JOINT_SIGNATURE_OF_SEVERAL", result.getData().get(0).getOperationRights().getSignature().getValidationRights().getId());
        assertEquals(format.getDespod(), result.getData().get(0).getOperationRights().getSignature().getValidationRights().getName());
        assertEquals(format.getLimope(), result.getData().get(0).getOperationRights().getSignature().getAmountLimit().getAmount());
        assertEquals(format.getDivlim(), result.getData().get(0).getOperationRights().getSignature().getAmountLimit().getCurrency());
        assertEquals(format.getNomtitu(), result.getData().get(0).getHolder().getFullName());
        assertEquals(format.getSaldisp(), result.getData().get(0).getBalance().getTotalAmount().getAmount());
        assertEquals(format.getDivcue(), result.getData().get(0).getBalance().getTotalAmount().getCurrency());
        assertEquals(format.getCodpro(), result.getData().get(0).getProduct().getId());
        assertEquals(format.getNompro(), result.getData().get(0).getProduct().getName());
        assertEquals("FUNDS", result.getData().get(0).getProduct().getProductType().getId());
        assertEquals(format.getDespro(), result.getData().get(0).getProduct().getProductType().getDescription());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        DTOIntServiceContract result = new DTOIntServiceContract();
        FormatoKNECLCS0 format = FormatsKwlcMock.getInstance().getFormatoKNECLCS0s().get(1);
        result = mapper.mapOut(format, result);
        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(1, result.getData().size());
        assertNotNull(result.getData().get(0));
        assertNull(result.getData().get(0).getId());
        assertNull(result.getData().get(0).getContractType());
        assertNull(result.getData().get(0).getNumber());
        assertNull(result.getData().get(0).getNumberType());
        assertNull(result.getData().get(0).getIsFavourite());
        assertNull(result.getData().get(0).getAlias());
        assertNull(result.getData().get(0).getCurrency());
        assertNull(result.getData().get(0).getCountry());
        assertNull(result.getData().get(0).getBank());
        assertNull(result.getData().get(0).getInternalContractNumber());
        assertNull(result.getData().get(0).getCounterparty());
        assertNull(result.getData().get(0).getHolder());
        assertNull(result.getData().get(0).getProduct());
        assertNotNull(result.getData().get(0).getOperationRights());
        assertNotNull(result.getData().get(0).getBalance());

        assertEquals(BigDecimal.ZERO, result.getData().get(0).getOperationRights().getSignature().getAmountLimit().getAmount());
        assertEquals(BigDecimal.ZERO, result.getData().get(0).getBalance().getTotalAmount().getAmount());
    }

    @Test
    public void mapOut2FullTest() throws IOException {
        FormatoKNECLCS1 format = FormatsKwlcMock.getInstance().getFormatoKNECLCS1();
        DTOIntServiceContract result = new DTOIntServiceContract();
        result = mapper.mapOut2(format, result);
        assertNotNull(result);
        assertNotNull(result.getPagination());
        assertNotNull(result.getPagination().getPaginationKey());
        assertNotNull(result.getPagination().getPageSize());
        assertEquals(format.getIdpagin(), result.getPagination().getPaginationKey());
        assertEquals(format.getTampagi().intValue(), result.getPagination().getPageSize().intValue());
    }

    @Test
    public void mapOut2EmptyTest() {
        DTOIntServiceContract result = new DTOIntServiceContract();
        result = mapper.mapOut2(new FormatoKNECLCS1(), result);
        assertNotNull(result);
        assertNull(result.getPagination());
    }
}
