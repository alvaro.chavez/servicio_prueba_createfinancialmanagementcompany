package com.bbva.pzic.financialmanagementcompanies.business.dto;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ServiceContract;

import java.util.List;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class DTOIntServiceContract {

    private List<ServiceContract> data;
    private DTOIntPagination pagination;

    public List<ServiceContract> getData() {
        return data;
    }

    public void setData(List<ServiceContract> data) {
        this.data = data;
    }

    public DTOIntPagination getPagination() {
        return pagination;
    }

    public void setPagination(DTOIntPagination pagination) {
        this.pagination = pagination;
    }
}
