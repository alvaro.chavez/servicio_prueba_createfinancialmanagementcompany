package com.bbva.pzic.financialmanagementcompanies.business.dto;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntBank {

    private String id;
    private DTOIntBranch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntBranch getBranch() {
        return branch;
    }

    public void setBranch(DTOIntBranch branch) {
        this.branch = branch;
    }
}