package com.bbva.pzic.financialmanagementcompanies.business.dto;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.RelatedContract;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 13/09/2018.
 *
 * @author Entelgy
 */
public class InputCreateFinancialManagementCompaniesRelatedContracts {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompaniesRelatedContracts.class)
    private String financialManagementCompanyId;
    @Valid
    private List<RelatedContract> relatedContracts;

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public List<RelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContract(List<RelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }
}
