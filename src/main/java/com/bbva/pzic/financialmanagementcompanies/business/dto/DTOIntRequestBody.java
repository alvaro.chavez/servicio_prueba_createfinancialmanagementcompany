package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.NotNull;

public class DTOIntRequestBody {

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String operation;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String country;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String dataOperationAlias;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class})
    private String dataOperationBank;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String dataOperationPdgroup;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String dataOptionalsLdap;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String dataOptionalsRama;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class,
            ValidationGroup.SendEmailOtpDeleteUser.class,
            ValidationGroup.SendEmailOtpDeleteGroup.class})
    private String dataOptionalsGrupo;

    private String dataOptionalsPrefijo;

    @NotNull(groups = {ValidationGroup.SendEmailOtpNewUserSimpleWithPasswordOnce.class})
    private String dataOperationPassword;
    @NotNull(groups = {ValidationGroup.SendEmailOtpReactivationUserSimpleWithPasswordOnce.class})
    private String dataOperationNewpassword;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDataOperationAlias() {
        return dataOperationAlias;
    }

    public void setDataOperationAlias(String dataOperationAlias) {
        this.dataOperationAlias = dataOperationAlias;
    }

    public String getDataOperationPassword() {
        return dataOperationPassword;
    }

    public void setDataOperationPassword(String dataOperationPassword) {
        this.dataOperationPassword = dataOperationPassword;
    }

    public String getDataOperationNewpassword() {
        return dataOperationNewpassword;
    }

    public void setDataOperationNewpassword(String dataOperationNewpassword) {
        this.dataOperationNewpassword = dataOperationNewpassword;
    }

    public String getDataOperationBank() {
        return dataOperationBank;
    }

    public void setDataOperationBank(String dataOperationBank) {
        this.dataOperationBank = dataOperationBank;
    }

    public String getDataOperationPdgroup() {
        return dataOperationPdgroup;
    }

    public void setDataOperationPdgroup(String dataOperationPdgroup) {
        this.dataOperationPdgroup = dataOperationPdgroup;
    }

    public String getDataOptionalsLdap() {
        return dataOptionalsLdap;
    }

    public void setDataOptionalsLdap(String dataOptionalsLdap) {
        this.dataOptionalsLdap = dataOptionalsLdap;
    }

    public String getDataOptionalsRama() {
        return dataOptionalsRama;
    }

    public void setDataOptionalsRama(String dataOptionalsRama) {
        this.dataOptionalsRama = dataOptionalsRama;
    }

    public String getDataOptionalsGrupo() {
        return dataOptionalsGrupo;
    }

    public void setDataOptionalsGrupo(String dataOptionalsGrupo) {
        this.dataOptionalsGrupo = dataOptionalsGrupo;
    }

    public String getDataOptionalsPrefijo() {
        return dataOptionalsPrefijo;
    }

    public void setDataOptionalsPrefijo(String dataOptionalsPrefijo) {
        this.dataOptionalsPrefijo = dataOptionalsPrefijo;
    }

}
