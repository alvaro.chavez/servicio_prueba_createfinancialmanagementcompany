package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntRelatedProduct {

    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private String id;
    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntProductType productType;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntProductType getProductType() {
        return productType;
    }

    public void setProductType(DTOIntProductType productType) {
        this.productType = productType;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
}