package com.bbva.pzic.financialmanagementcompanies.business.dto;


/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public class InputGetSubscriptionRequest {

    private String subscriptionRequestId;

    public String getSubscriptionRequestId() {
        return subscriptionRequestId;
    }

    public void setSubscriptionRequestId(String subscriptionRequestId) {
        this.subscriptionRequestId = subscriptionRequestId;
    }
}