package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntBusiness {

    private String id;
    @Valid
    private List<DTOIntBusinessDocument> businessDocuments;
    @Valid
    @NotNull(groups = ValidationGroup.CreateFinancialManagementCompany.class)
    private DTOIntBusinessManagement businessManagement;
    @Valid
    private DTOIntLimitAmount limitAmount;

    public List<DTOIntBusinessDocument> getBusinessDocuments() {
        return businessDocuments;
    }

    public void setBusinessDocuments(
            List<DTOIntBusinessDocument> businessDocuments) {
        this.businessDocuments = businessDocuments;
    }

    public DTOIntBusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public void setBusinessManagement(
            DTOIntBusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public DTOIntLimitAmount getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(DTOIntLimitAmount limitAmount) {
        this.limitAmount = limitAmount;
    }

}