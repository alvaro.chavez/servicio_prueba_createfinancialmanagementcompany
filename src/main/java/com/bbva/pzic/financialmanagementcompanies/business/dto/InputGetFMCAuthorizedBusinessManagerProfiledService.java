package com.bbva.pzic.financialmanagementcompanies.business.dto;

import javax.validation.constraints.Size;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public class InputGetFMCAuthorizedBusinessManagerProfiledService {

    @Size(max = 8, groups = ValidationGroup.GetFMCAuthorizedBusinessManagerProfiledService.class)
    private String financialManagementCompanyId;

    @Size(max = 8, groups = ValidationGroup.GetFMCAuthorizedBusinessManagerProfiledService.class)
    private String authorizedBusinessManagerId;

    @Size(max = 4, groups = ValidationGroup.GetFMCAuthorizedBusinessManagerProfiledService.class)
    private String profiledServiceId;

    public String getFinancialManagementCompanyId() {
        return financialManagementCompanyId;
    }

    public void setFinancialManagementCompanyId(String financialManagementCompanyId) {
        this.financialManagementCompanyId = financialManagementCompanyId;
    }

    public String getAuthorizedBusinessManagerId() {
        return authorizedBusinessManagerId;
    }

    public void setAuthorizedBusinessManagerId(String authorizedBusinessManagerId) {
        this.authorizedBusinessManagerId = authorizedBusinessManagerId;
    }

    public String getProfiledServiceId() {
        return profiledServiceId;
    }

    public void setProfiledServiceId(String profiledServiceId) {
        this.profiledServiceId = profiledServiceId;
    }
}
