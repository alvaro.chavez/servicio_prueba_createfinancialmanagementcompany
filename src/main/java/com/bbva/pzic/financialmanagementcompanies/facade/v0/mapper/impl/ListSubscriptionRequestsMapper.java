package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputListSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.IListSubscriptionRequestsMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class ListSubscriptionRequestsMapper implements IListSubscriptionRequestsMapper {

    private static final Log LOG = LogFactory.getLog(ListSubscriptionRequestsMapper.class);

    @Autowired
    private EnumMapper enumMapper;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputListSubscriptionRequests mapIn(final String subscriptionsRequestId,
                                               final String businessDocumentsBusinessDocumentTypeId,
                                               final String businessDocumentsDocumentNumber,
                                               final String statusId, final String unitManagement,
                                               final String branchId, final String fromSubscriptionRequestDate,
                                               final String toSubscriptionRequestDate, final String businessId,
                                               final String paginationKey, final Integer pageSize) {
        LOG.info("... called method ListSubscriptionRequestsMapper.mapIn ...");
        InputListSubscriptionRequests input = new InputListSubscriptionRequests();
        input.setSubscriptionsRequestId(subscriptionsRequestId);
        input.setBusinessDocumentsBusinessDocumentTypeId(enumMapper.getBackendValue("subscriptionRequests.documentType.id", businessDocumentsBusinessDocumentTypeId));
        input.setBusinessDocumentsDocumentNumber(businessDocumentsDocumentNumber);
        input.setStatusId(enumMapper.getBackendValue("suscriptionRequest.status.id", statusId));
        input.setUnitManagement(unitManagement);
        input.setBranchId(branchId);
        input.setFromSubscriptionRequestDate(fromSubscriptionRequestDate);
        input.setToSubscriptionRequestDate(toSubscriptionRequestDate);
        input.setBusinessId(businessId);
        input.setPaginationKey(paginationKey);
        input.setPageSize(pageSize);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SubscriptionRequests mapOut(final DTOIntSubscriptionRequests dtoIntSubscriptionRequests) {
        LOG.info("... called method ListSubscriptionRequestsMapper.mapOut ...");
        if (dtoIntSubscriptionRequests == null) {
            return null;
        }
        SubscriptionRequests subscriptionRequests = new SubscriptionRequests();
        subscriptionRequests.setData(dtoIntSubscriptionRequests.getData());
        return subscriptionRequests;
    }
}
