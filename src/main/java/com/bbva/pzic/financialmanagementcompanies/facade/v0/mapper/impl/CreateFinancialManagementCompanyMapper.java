package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.ICreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.util.Enums;
import com.bbva.pzic.routine.translator.facade.Translator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CreateFinancialManagementCompanyMapper implements ICreateFinancialManagementCompanyMapper {

    private static final Log LOG = LogFactory.getLog(CreateFinancialManagementCompanyMapper.class);
    @Autowired
    private Translator translator;
    @Override
    public DTOIntFinancialManagementCompanies mapIn(final FinancialManagementCompanies financialManagementCompanies) {
        LOG.info("... called method CreateFinancialManagementCompanyMapper.mapIn ...");
        if(financialManagementCompanies == null) {
            return null;
        }
        DTOIntFinancialManagementCompanies dtoIn = new DTOIntFinancialManagementCompanies();
        dtoIn.setBusiness(mapInBusiness(financialManagementCompanies.getBusiness()));
        dtoIn.setNetcashType(mapInNetCashType(financialManagementCompanies.getNetcashType()));
        dtoIn.setContract(mapInContract(financialManagementCompanies.getContract()));
        dtoIn.setProduct(mapInProduct(financialManagementCompanies.getProduct()));
        dtoIn.setRelationType(mapInRelationType(financialManagementCompanies.getRelationType()));
        dtoIn.setReviewers(mapInReviewers(financialManagementCompanies.getReviewers()));

        return dtoIn;
    }

    private List<DTOIntReviewerNetcash> mapInReviewers(final List<ReviewerNetcash> reviewers) {
        if(CollectionUtils.isEmpty(reviewers)){
            return null;
        }
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private DTOIntReviewerNetcash mapInReviewer(final ReviewerNetcash reviewerNetcash) {
        DTOIntReviewerNetcash dtoIn = new DTOIntReviewerNetcash();
        dtoIn.setBusinessAgentId(reviewerNetcash.getBusinessAgentId());
        dtoIn.setProfessionPosition(reviewerNetcash.getProfessionPosition());
        dtoIn.setRegistrationIdentifier(reviewerNetcash.getRegistrationIdentifier());
        dtoIn.setUnitManagement(reviewerNetcash.getUnitManagement());
        dtoIn.setProfile(mapInProfile(reviewerNetcash.getProfile()));
        dtoIn.setBank(mapInBank(reviewerNetcash.getBank()));
        dtoIn.setContactDetails(mapInContractDetails(reviewerNetcash.getContactDetails()));
        dtoIn.setReviewerType(mapInReviewerType(reviewerNetcash.getReviewerType()));
        return dtoIn;
    }

    private DTOIntReviewerType mapInReviewerType(ReviewerType reviewerType) {
        if(reviewerType == null) {
            return null;
        }
        DTOIntReviewerType dtoIn = new DTOIntReviewerType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_REVIEWER_ID,reviewerType.getId()));
        return dtoIn;
    }

    private List<DTOIntContactDetail> mapInContractDetails(final List<ContactDetail> contactDetails) {
        if(CollectionUtils.isEmpty(contactDetails)){
            return null;
        }
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContractDetail).collect(Collectors.toList());
    }

    private DTOIntContactDetail mapInContractDetail(final ContactDetail contactDetail) {
        DTOIntContactDetail dtoIn = new DTOIntContactDetail();
        dtoIn.setContact(contactDetail.getContact());
        dtoIn.setContactType(translator.translateFrontendEnumValueStrictly(Enums.ENUM_CONTACTDETAILS_CONTACTTYPE_ID, contactDetail.getContactType()));
        return dtoIn;
    }

    private DTOIntBank mapInBank(final Bank bank) {
        if(bank == null){
            return null;
        }
        DTOIntBank dtoIn = new DTOIntBank();
        dtoIn.setId(bank.getId());
        dtoIn.setBranch(mapInBranch(bank.getBranch()));
        return dtoIn;
    }

    private DTOIntBranch mapInBranch(final Branch branch) {
        if(branch == null){
            return null;
        }
        DTOIntBranch dtoIn = new DTOIntBranch();
        dtoIn.setId(branch.getId());
        return dtoIn;
    }

    private DTOIntProfile mapInProfile(final Profile profile) {
        if(profile == null){
            return null;
        }
        DTOIntProfile dtoIn = new DTOIntProfile();
        dtoIn.setId(profile.getId());
        return dtoIn;
    }


    private DTOIntRelationType mapInRelationType(final RelationType relationType) {
        if(relationType == null){
            return null;
        }
        DTOIntRelationType dtoIn = new DTOIntRelationType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_RELATEDCONTRACTS_RELATIONTYPE,relationType.getId()));
        return dtoIn;
    }

    private DTOIntRelatedProduct mapInProduct(final RelatedProduct product) {
        if(product == null){
            return null;
        }
        DTOIntRelatedProduct dtoIn = new DTOIntRelatedProduct();
        dtoIn.setId(product.getId());
        dtoIn.setProductType(mapInProductType(product.getProductType()));
        return dtoIn;
    }

    private DTOIntProductType mapInProductType(final ProductType productType) {
        if(productType == null){
            return null;
        }
        DTOIntProductType dtoIn=new DTOIntProductType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_PRODUCTTYPE_ID,productType.getId()));
        return dtoIn;
    }

    private DTOIntContract mapInContract(final Contract contract) {
        if(contract == null){
            return null;
        }
        DTOIntContract dtoIn = new DTOIntContract();
        dtoIn.setId(contract.getId());
        return dtoIn;
    }

    private DTOIntNetcashType mapInNetCashType(final NetcashType netcashType) {
        if (netcashType == null) {
            return null;
        }
        DTOIntNetcashType dtoIn = new DTOIntNetcashType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_SUSCRIPTIONREQUEST_PRODUCT_ID,netcashType.getId()));
        dtoIn.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoIn;
    }

    private DTOIntVersionProduct mapInVersion(final Version version) {
        if(version == null){
            return null;
        }
        DTOIntVersionProduct dtoIn = new DTOIntVersionProduct();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_VERSION_ID,version.getId()));
        return dtoIn;
    }

    private DTOIntBusiness mapInBusiness(final Business business) {
        if(business == null){
            return null;
        }
        DTOIntBusiness dtoIn = new DTOIntBusiness();
        dtoIn.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        dtoIn.setBusinessManagement(mapInbusinessManagement(business.getBusinessManagement()));
        dtoIn.setLimitAmount(mapInlimitAmount(business.getLimitAmount()));
        return dtoIn;
    }

    private DTOIntLimitAmount mapInlimitAmount(final LimitAmount limitAmount) {
        if(limitAmount == null){
            return null;
        }
        DTOIntLimitAmount dtoIn = new DTOIntLimitAmount();
        dtoIn.setAmount(limitAmount.getAmount());
        dtoIn.setCurrency(limitAmount.getCurrency());
        return dtoIn;
    }

    private List<DTOIntBusinessDocument> mapInBusinessDocuments(final List<BusinessDocument> businessDocuments) {
        if(CollectionUtils.isEmpty(businessDocuments)){
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }

    private DTOIntBusinessDocument mapInBusinessDocument(final BusinessDocument businessDocument) {
        DTOIntBusinessDocument dtoIn = new DTOIntBusinessDocument();
        dtoIn.setDocumentNumber(businessDocument.getDocumentNumber());
        dtoIn.setExpirationDate(businessDocument.getExpirationDate());
        dtoIn.setIssueDate(businessDocument.getIssueDate());
        dtoIn.setBusinessDocumentType(mapBusinessDocumentType(businessDocument.getBusinessDocumentType()));
        return dtoIn;
    }

    private DTOIntBusinessDocumentType mapBusinessDocumentType(final BusinessDocumentType businessDocumentType) {
        if(businessDocumentType == null){
            return null;
        }
        DTOIntBusinessDocumentType dtoIn= new DTOIntBusinessDocumentType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_DOCUMENTTYPE_ID,businessDocumentType.getId()));
        return dtoIn;
    }

    private DTOIntBusinessManagement mapInbusinessManagement(final BusinessManagement businessManagement) {
        if(businessManagement == null){
            return null;
        }
        DTOIntBusinessManagement dtoIn = new DTOIntBusinessManagement();
        dtoIn.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return dtoIn;
    }

    private DTOIntManagementType mapInManagementType(final ManagementType managementType) {
        if(managementType == null){
            return null;
        }
        DTOIntManagementType dtoIn = new DTOIntManagementType();
        dtoIn.setId(translator.translateFrontendEnumValueStrictly(Enums.ENUM_FINANCIALMANAGEMENTCOMPANY_BUSINESSMANAGEMENT_MANAGEMENTTYPE_ID,managementType.getId()));
        return dtoIn;
    }

    @Override
    public ServiceResponse<FinancialManagementCompanies> mapOut(final FinancialManagementCompanies input) {
        LOG.info("... called method CreateFinancialManagementCompanyMapper.mapOut ...");
        if(input == null)
            return null;
        return ServiceResponse.data(input).build();
    }
}
