package com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifySubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.SubscriptionRequest;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
public interface IModifySubscriptionRequestMapper {

    InputModifySubscriptionRequest mapIn(String subscriptionRequestId,
                                         SubscriptionRequest subscriptionRequest);
}
