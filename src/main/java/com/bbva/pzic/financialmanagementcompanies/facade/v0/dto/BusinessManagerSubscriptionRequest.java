package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "businessManagerSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "businessManagerSubscriptionRequest", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessManagerSubscriptionRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Manager first name. DISCLAIMER: It will be mandatory when fullName
     * attribute is not fulfilled.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @DatoAuditable(omitir = true)
    private String firstName;
    /**
     * Manager middle name.
     */
    @DatoAuditable(omitir = true)
    private String middleName;
    /**
     * Manager last name.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @DatoAuditable(omitir = true)
    private String lastName;
    /**
     * Manager second last name.
     */
    @DatoAuditable(omitir = true)
    private String secondLastName;
    /**
     * Manager identity documents.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private List<IdentityDocumentSubscriptionRequest> identityDocuments;
    /**
     * Manager contact details.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private List<ContactDetail> contactDetails;
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    @Valid
    private List<Role> roles;
    /**
     * Customer unique identifier.
     */
    private String id;
    /**
     * Customer identifier for channel control purposes:
     * "channel + internal bank + reference + user".
     */
    @DatoAuditable(omitir = true)
    private String targetUserId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public List<IdentityDocumentSubscriptionRequest> getIdentityDocuments() {
        return identityDocuments;
    }

    public void setIdentityDocuments(
            List<IdentityDocumentSubscriptionRequest> identityDocuments) {
        this.identityDocuments = identityDocuments;
    }

    public List<ContactDetail> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetail> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }
}
