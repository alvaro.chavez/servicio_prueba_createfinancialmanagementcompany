package com.bbva.pzic.financialmanagementcompanies.facade.v0.impl;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.catalog.gabi.Pagination;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.servicing.annotations.PATCH;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;
import com.bbva.jee.arq.spring.core.servicing.utils.toolkit.BusinessServicesToolkitManager;
import com.bbva.pzic.financialmanagementcompanies.business.ISrvIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntServiceContract;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntSubscriptionRequests;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputRequestBackendRest;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.ksjo.ResponseData;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.ISrvFinancialManagementCompaniesV0;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.*;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.mapper.*;
import com.bbva.pzic.financialmanagementcompanies.util.BusinessServiceUtil;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.GabiPaginationMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.PaginationMapper;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.financialmanagementcompanies.facade.RegistryIds.*;

/**
 * Created on 03/08/2018.
 *
 * @author Entelgy
 */
@Path("/v0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = "SNPE1800087", logicalID = "financialManagementCompanies")
@VN(vnn = "v0")
@Service
public class SrvFinancialManagementCompaniesV0 implements ISrvFinancialManagementCompaniesV0, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static final Log LOG = LogFactory.getLog(SrvFinancialManagementCompaniesV0.class);

    private static final String FINANCIAL_MANAGEMENT_COMPANY_ID = "financial-management-company-id";
    private static final String AUTHORIZED_BUSINESS_MANAGER_ID = "authorized-business-manager-id";
    private static final String PROFILED_SERVICE_ID = "profiled-service-id";

    public HttpHeaders httpHeaders;
    public UriInfo uriInfo;

    @Deprecated
    @Autowired
    private BusinessServicesToolKit businessServicesToolKit;
    @Autowired
    private BusinessServicesToolkitManager businessServicesToolkitManager;

    @Autowired
    private ISrvIntFinancialManagementCompanies srvIntFinancialManagementCompanies;
    @Autowired
    private ICreateSubscriptionRequestMapper createSubscriptionRequestMapper;
    @Autowired
    private IListSubscriptionRequestsMapper listSubscriptionRequestsMapper;
    @Autowired
    private IGetSubscriptionRequestMapper getSubscriptionRequestMapper;
    @Autowired
    private IModifySubscriptionRequestMapper modifySubscriptionRequestMapper;
    @Autowired
    private IModifyBusinessManagerSubscriptionRequestMapper modifyBusinessManagerSubscriptionRequestMapper;
    @Autowired
    private ICreateReviewerSubscriptionRequestMapper createReviewerSubscriptionRequestMapper;
    @Autowired
    private ICreateFinancialManagementCompaniesRelatedContractsMapper createFinancialManagementCompaniesRelatedContractsMapper;
    @Autowired
    private ICreateFinancialManagementCompaniesBusinessManagerMapper createFinancialManagementCompaniesBusinessManagerMapper;
    @Autowired
    private ICreateFinancialManagementCompanyMapper createFinancialManagementCompanyMapper;
    @Autowired
    private ICreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper createFinancialManagementCompaniesAuthorizedBusinessManagerMapper;
    @Autowired
    private ISendEmailOtpFinancialManagementCompaniesBusinessManagerMapper sendEmailOtpMapper;
    @Autowired
    private IGetFMCAuthorizedBusinessManagerProfiledServiceMapper getFMCAuthorizedBusinessManagerProfiledServiceMapper;
    @Autowired
    private IValidateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper validateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper;
    @Autowired
    private IListFMCAuthorizedBusinessManagersProfiledServicesContractsMapper listFMCAuthorizedBusinessManagersProfiledServicesContractsMapper;
    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Override
    public void setUriInfo(UriInfo uriInfo) {
        this.uriInfo = uriInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/subscription-requests")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_SUBSCRIPTION_REQUEST, logicalID = "createSubscriptionRequest")
    public ServiceResponse<SubscriptionRequestId> createSubscriptionRequest(
            final SubscriptionRequest subscriptionRequest) {
        LOG.info("----- Invoking service createSubscriptionRequest -----");
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_SUBSCRIPTION_REQUEST, subscriptionRequest, null, null);
        ServiceResponse<SubscriptionRequestId> serviceResponse = createSubscriptionRequestMapper.mapOut(
                srvIntFinancialManagementCompanies.createSubscriptionRequest(
                        createSubscriptionRequestMapper.mapIn(subscriptionRequest)));
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_SUBSCRIPTION_REQUEST, serviceResponse, null, null);
        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/subscription-requests")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_SUBSCRIPTION_REQUESTS, logicalID = "listSubscriptionRequests")
    public SubscriptionRequests listSubscriptionRequests(
            @QueryParam("subscriptionsRequest.id") final String subscriptionsRequestId,
            @QueryParam("businessDocuments.businessDocumentType.id") final String businessDocumentsBusinessDocumentTypeId,
            @QueryParam("businessDocuments.documentNumber") @DatoAuditable(omitir = true) final String businessDocumentsDocumentNumber,
            @QueryParam("status.id") final String statusId,
            @QueryParam("unitManagement") final String unitManagement,
            @QueryParam("branch.id") final String branchId,
            @QueryParam("fromSubscriptionRequestDate") final String fromSubscriptionRequestDate,
            @QueryParam("toSubscriptionRequestDate") final String toSubscriptionRequestDate,
            @QueryParam("business.id") @DatoAuditable(omitir = true) final String businessId,
            @QueryParam("expand") final String expand,
            @QueryParam("paginationKey") final String paginationKey,
            @QueryParam("pageSize") final Integer pageSize) {
        LOG.info("----- Invoking service listSubscriptionRequests -----");

        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put("subscriptionsRequest.id", subscriptionsRequestId);
        queryParam.put("businessDocuments.businessDocumentType.id", businessDocumentsBusinessDocumentTypeId);
        queryParam.put("businessDocuments.documentNumber", businessDocumentsDocumentNumber);
        queryParam.put("status.id", statusId);
        queryParam.put("unitManagement", unitManagement);
        queryParam.put("branch.id", branchId);
        queryParam.put("fromSubscriptionRequestDate", fromSubscriptionRequestDate);
        queryParam.put("toSubscriptionRequestDate", toSubscriptionRequestDate);
        queryParam.put("business.id", businessId);
        queryParam.put("expand", expand);
        queryParam.put("paginationKey", paginationKey);
        queryParam.put("pageSize", pageSize);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_SUBSCRIPTION_REQUESTS, null, null, queryParam);
        DTOIntSubscriptionRequests dtoInt = srvIntFinancialManagementCompanies.listSubscriptionRequests(
                listSubscriptionRequestsMapper.mapIn(
                        (String) queryParam.get("subscriptionsRequest.id"),
                        (String) queryParam.get("businessDocuments.businessDocumentType.id"),
                        (String) queryParam.get("businessDocuments.documentNumber"),
                        (String) queryParam.get("status.id"),
                        (String) queryParam.get("unitManagement"),
                        (String) queryParam.get("branch.id"),
                        (String) queryParam.get("fromSubscriptionRequestDate"),
                        (String) queryParam.get("toSubscriptionRequestDate"),
                        (String) queryParam.get("business.id"),
                        (String) queryParam.get("paginationKey"),
                        (Integer) queryParam.get("pageSize")
                ));

        SubscriptionRequests subscriptionRequests = listSubscriptionRequestsMapper.mapOut(dtoInt);
        if (subscriptionRequests == null) {
            return null;
        }

        BusinessServiceUtil.expand(subscriptionRequests.getData(),
                "reviewers", (String) queryParam.get("expand"));

        if (dtoInt.getPagination() != null) {
            subscriptionRequests.setPagination(PaginationMapper.build(
                    businessServicesToolKit.getPaginationBuider().setPagination(
                            SrvFinancialManagementCompaniesV0.class, "listSubscriptionRequests", uriInfo,
                            dtoInt.getPagination().getPaginationKey(),
                            dtoInt.getPagination().getPrevious(),
                            dtoInt.getPagination().getPageSize(), dtoInt.getPagination().getPage(), dtoInt.getPagination().getTotalPages(), null, null, dtoInt.getPagination().getTotalElements())
                            .build()));
        }
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_SUBSCRIPTION_REQUESTS, subscriptionRequests, null, null);
        return subscriptionRequests;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/subscription-requests/{subscription-request-id}")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_SUBSCRIPTION_REQUEST, logicalID = "getSubscriptionRequest")
    public ServiceResponse<SubscriptionRequest> getSubscriptionRequest(
            @PathParam("subscription-request-id") final String subscriptionRequestId,
            @QueryParam("expand") final String expand) {
        LOG.info("----- Invoking service getSubscriptionRequest -----");

        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put("subscription-request-id", subscriptionRequestId);
        Map<String, Object> queryParam = new HashMap<>();
        queryParam.put("expand", expand);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_SUBSCRIPTION_REQUEST, null, pathParam, queryParam);
        SubscriptionRequest subscriptionRequest = srvIntFinancialManagementCompanies.getSubscriptionRequest(
                getSubscriptionRequestMapper.mapIn(
                        (String) pathParam.get("subscription-request-id")
                ));

        if (subscriptionRequest == null) {
            return null;
        }
        BusinessServiceUtil.expand(subscriptionRequest, "reviewers,businessManagers",
                expand == null ? null : expand.replace("-m", "M"));
        ServiceResponse<SubscriptionRequest> serviceResponse = getSubscriptionRequestMapper.mapOut(subscriptionRequest);
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_SUBSCRIPTION_REQUEST, serviceResponse, null, null);
        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PATCH
    @Path("/subscription-requests/{subscription-request-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_SUBSCRIPTION_REQUEST, logicalID = "modifySubscriptionRequest")
    public void modifySubscriptionRequest(
            @PathParam("subscription-request-id") final String subscriptionRequestId,
            final SubscriptionRequest subscriptionRequest) {
        LOG.info("----- Invoking service modifySubscriptionRequest -----");

        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put("subscription-request-id", subscriptionRequestId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_SUBSCRIPTION_REQUEST, subscriptionRequest, pathParam, null);
        srvIntFinancialManagementCompanies.modifySubscriptionRequest(
                modifySubscriptionRequestMapper.mapIn(
                        (String) pathParam.get("subscription-request-id"),
                        subscriptionRequest
                ));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @PATCH
    @Path("/subscription-requests/{subscription-request-id}/business-managers/{business-manager-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_MODIFY_BUSINESS_MANAGER_SUBSCRIPTION_REQUEST, logicalID = "modifyBusinessManagerSubscriptionRequest")
    public void modifyBusinessManagerSubscriptionRequest(
            @PathParam("subscription-request-id") final String subscriptionRequestId,
            @PathParam("business-manager-id") final String businessManagerId,
            final BusinessManagerSubscriptionRequest businessManagerSubscriptionRequest) {
        LOG.info("----- Invoking service modifyBusinessManagerSubscriptionRequest -----");

        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put("subscription-request-id", subscriptionRequestId);
        pathParam.put("business-manager-id", businessManagerId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_MODIFY_BUSINESS_MANAGER_SUBSCRIPTION_REQUEST, businessManagerSubscriptionRequest, pathParam, null);
        srvIntFinancialManagementCompanies.modifyBusinessManagerSubscriptionRequest(
                modifyBusinessManagerSubscriptionRequestMapper.mapIn(
                        (String) pathParam.get("subscription-request-id"),
                        (String) pathParam.get("business-manager-id"),
                        businessManagerSubscriptionRequest));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/subscription-requests/{subscription-request-id}/reviewers")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_REVIEWER_SUBSCRIPTION_REQUEST, logicalID = "createReviewerSubscriptionRequest")
    public ServiceResponse<ReviewerId> createReviewerSubscriptionRequest(
            @PathParam("subscription-request-id") final String subscriptionRequestId,
            final Reviewer reviewer) {
        LOG.info("----- Invoking service createReviewerSubscriptionRequest -----");
        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put("subscription-request-id", subscriptionRequestId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_REVIEWER_SUBSCRIPTION_REQUEST, reviewer, pathParam, null);
        ServiceResponse<ReviewerId> serviceResponse = createReviewerSubscriptionRequestMapper.mapOut(
                srvIntFinancialManagementCompanies.createReviewerSubscriptionRequest(
                        createReviewerSubscriptionRequestMapper.mapIn(
                                (String) pathParam.get("subscription-request-id"),
                                reviewer)));
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_REVIEWER_SUBSCRIPTION_REQUEST, serviceResponse, null, null);
        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/financial-management-companies/{financial-management-company-id}/related-contracts-bulk")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_RELATED_CONTRACTS, logicalID = "createFinancialManagementCompaniesRelatedContracts")
    public Response createFinancialManagementCompaniesRelatedContracts(
            @PathParam(FINANCIAL_MANAGEMENT_COMPANY_ID) final String financialManagementCompanyId,
            final List<RelatedContract> relatedContract) {
        LOG.info("----- Invoking service createFinancialManagementCompaniesRelatedContracts -----");
        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put(FINANCIAL_MANAGEMENT_COMPANY_ID, financialManagementCompanyId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_RELATED_CONTRACTS, relatedContract, pathParam, null);
        srvIntFinancialManagementCompanies.createFinancialManagementCompaniesRelatedContracts(
                createFinancialManagementCompaniesRelatedContractsMapper.mapIn(
                        (String) pathParam.get(FINANCIAL_MANAGEMENT_COMPANY_ID),
                        relatedContract));
        return Response.status(Response.Status.SEE_OTHER).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/business-managers")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_BUSINESS_MANAGER, logicalID = "createFinancialManagementCompaniesBusinessManager")
    public ServiceResponse<BusinessManagerId> createFinancialManagementCompaniesBusinessManager(
            final BusinessManager businessManager) {
        LOG.info("----- Invoking service createFinancialManagementCompaniesBusinessManager -----");
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_BUSINESS_MANAGER, businessManager, null, null);
        ServiceResponse<BusinessManagerId> serviceResponse = createFinancialManagementCompaniesBusinessManagerMapper.mapOut(
                srvIntFinancialManagementCompanies.createFinancialManagementCompaniesBusinessManager(
                        createFinancialManagementCompaniesBusinessManagerMapper.mapIn(businessManager)));
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_BUSINESS_MANAGER, serviceResponse, null, null);
        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/financial-management-companies/{financial-management-company-id}/authorized-business-managers")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_AUTHORIZED_BUSINESS_MANAGER, logicalID = "createFinancialManagementCompaniesAuthorizedBusinessManager")
    public ServiceResponse<CreateAuthorizedBusinessManager> createFinancialManagementCompaniesAuthorizedBusinessManager(
            @PathParam(FINANCIAL_MANAGEMENT_COMPANY_ID) final String financialManagementCompanyId,
            final CreateAuthorizedBusinessManager createAuthorizedBusinessManager) {
        LOG.info("----- Invoking service createFinancialManagementCompaniesAuthorizedBusinessManager -----");

        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put(FINANCIAL_MANAGEMENT_COMPANY_ID, financialManagementCompanyId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_AUTHORIZED_BUSINESS_MANAGER, createAuthorizedBusinessManager, pathParam, null);
        ServiceResponse<CreateAuthorizedBusinessManager> serviceResponse = createFinancialManagementCompaniesAuthorizedBusinessManagerMapper.mapOut(
                srvIntFinancialManagementCompanies.createFinancialManagementCompaniesAuthorizedBusinessManager(
                        createFinancialManagementCompaniesAuthorizedBusinessManagerMapper.mapIn(
                                (String) pathParam.get(FINANCIAL_MANAGEMENT_COMPANY_ID),
                                createAuthorizedBusinessManager
                        )));
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANIES_AUTHORIZED_BUSINESS_MANAGER, serviceResponse, null, null);
        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/business-managers/{business-manager-id}/send-email-otp")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_SEND_EMAIL_OTP_FINANCIAL_MANAGEMENT_COMPANIES_BUSINESS_MANAGER, logicalID = "sendEmailOtpFinancialManagementCompaniesBusinessManager")
    public void sendEmailOtpFinancialManagementCompaniesBusinessManager(
            @PathParam("business-manager-id") final String businessManagerId,
            final BusinessManager businessManager) {
        LOG.info("----- Invoking service sendEmailOtpFinancialManagementCompaniesBusinessManager -----");
        Map<String, Object> pathParam = new HashMap<>();
        pathParam.put("business-manager-id", businessManagerId);
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEND_EMAIL_OTP_FINANCIAL_MANAGEMENT_COMPANIES_BUSINESS_MANAGER, businessManager, pathParam, null);

        InputRequestBackendRest inputRequestBackendRest = sendEmailOtpMapper.mapIn(
                (String) pathParam.get("business-manager-id")
        );

        // KSJO - OP51
        ResponseData responseData = srvIntFinancialManagementCompanies.sendEmailOtpNewUserSimpleWithPasswordOnce(inputRequestBackendRest);
        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_SEND_EMAIL_OTP_FINANCIAL_MANAGEMENT_COMPANIES_BUSINESS_MANAGER, responseData, null, null);
        if ("MDO_008".equals(responseData.getStatusCode())) {
            // KSJO - OP52
            srvIntFinancialManagementCompanies.sendEmailOtpReactivationUserSimpleWithPasswordOnce(
                    sendEmailOtpMapper.mapInReactivate(inputRequestBackendRest));
        }

        try {
            // invoke KWHF
            InputSendEmailOtpFinancialManagementCompaniesBusinessManager emails =
                    srvIntFinancialManagementCompanies.sendEmailOtpGetBusinessManagerData(
                            sendEmailOtpMapper.mapInGetBusinessManagerData(
                                    (String) pathParam.get("business-manager-id"),
                                    inputRequestBackendRest
                            ));

            srvIntFinancialManagementCompanies.sendEmailOtpFinancialManagementCompaniesBusinessManager(
                    sendEmailOtpMapper.mapInSendEmail(
                            (String) pathParam.get("business-manager-id"),
                            inputRequestBackendRest,
                            emails)
            );
        } catch (ExcepcionRespuestaHost ex) {
            LOG.error("Error recibido desde backend host KWHF");
            // op 15
            srvIntFinancialManagementCompanies.sendEmailOtpDeleteUser(
                    sendEmailOtpMapper.mapInDelUser(inputRequestBackendRest));

            // op 16
            srvIntFinancialManagementCompanies.sendEmailOtpDeleteGroup(
                    sendEmailOtpMapper.mapInDelFromGroupUser(inputRequestBackendRest));

            throw ex;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/financial-management-companies/{financial-management-company-id}/authorized-business-managers/{authorized-business-manager-id}/profiled-services/{profiled-service-id}")
    @SMC(registryID = SMC_REGISTRY_ID_OF_GET_FMC_AUTHORIZED_BUSINESS_MANAGER_PROFILED_SERVICE, logicalID = "getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService")
    public ServiceResponse<ProfiledService> getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
            @DatoAuditable(omitir = true) @PathParam(FINANCIAL_MANAGEMENT_COMPANY_ID) final String financialManagementCompanyId,
            @DatoAuditable(omitir = true) @PathParam(AUTHORIZED_BUSINESS_MANAGER_ID) final String authorizedBusinessManagerId,
            @DatoAuditable(omitir = true) @PathParam(PROFILED_SERVICE_ID) final String profiledServiceId) {
        LOG.info("----- Invoking service getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(FINANCIAL_MANAGEMENT_COMPANY_ID, financialManagementCompanyId);
        pathParams.put(AUTHORIZED_BUSINESS_MANAGER_ID, authorizedBusinessManagerId);
        pathParams.put(PROFILED_SERVICE_ID, profiledServiceId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_FMC_AUTHORIZED_BUSINESS_MANAGER_PROFILED_SERVICE, null, pathParams, null);

        ServiceResponse<ProfiledService> serviceResponse = getFMCAuthorizedBusinessManagerProfiledServiceMapper.mapOut(
                srvIntFinancialManagementCompanies.getFinancialManagementCompaniesAuthorizedBusinessManagerProfiledService(
                        getFMCAuthorizedBusinessManagerProfiledServiceMapper.mapIn(
                                pathParams.get(FINANCIAL_MANAGEMENT_COMPANY_ID).toString(),
                                pathParams.get(AUTHORIZED_BUSINESS_MANAGER_ID).toString(),
                                pathParams.get(PROFILED_SERVICE_ID).toString())));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_GET_FMC_AUTHORIZED_BUSINESS_MANAGER_PROFILED_SERVICE, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @POST
    @Path("/financial-management-companies/{financial-management-company-id}/authorized-business-managers/{authorized-business-manager-id}/profiled-services/{profiled-service-id}/validate-operation-feasibility")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_VALIDATE_FMC_AUTHORIZED_BUSINESS_MANAGER_PROFILED_SERVICE_VALIDATE_OPERATION_FEASIBILITY, logicalID = "validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility")
    public ServiceResponse<ValidateOperationFeasibility> validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
            @DatoAuditable(omitir = true) @PathParam(FINANCIAL_MANAGEMENT_COMPANY_ID) final String financialManagementCompanyId,
            @DatoAuditable(omitir = true) @PathParam(AUTHORIZED_BUSINESS_MANAGER_ID) final String authorizedBusinessManagerId,
            @PathParam(PROFILED_SERVICE_ID) final String profiledServiceId,
            final ValidateOperationFeasibility validateOperationFeasibility) {
        LOG.info("----- Invoking service validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(FINANCIAL_MANAGEMENT_COMPANY_ID, financialManagementCompanyId);
        pathParams.put(AUTHORIZED_BUSINESS_MANAGER_ID, authorizedBusinessManagerId);
        pathParams.put(PROFILED_SERVICE_ID, profiledServiceId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_VALIDATE_FMC_AUTHORIZED_BUSINESS_MANAGER_PROFILED_SERVICE_VALIDATE_OPERATION_FEASIBILITY, validateOperationFeasibility, pathParams, null);

        ServiceResponse<ValidateOperationFeasibility> serviceResponse = validateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper.mapOut(
                srvIntFinancialManagementCompanies.validateFinancialManagementCompaniesAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibility(
                        validateFMCAuthorizedBusinessManagerProfiledServiceValidateOperationFeasibilityMapper.mapIn(
                                pathParams.get(FINANCIAL_MANAGEMENT_COMPANY_ID).toString(),
                                pathParams.get(AUTHORIZED_BUSINESS_MANAGER_ID).toString(),
                                pathParams.get(PROFILED_SERVICE_ID).toString(),
                                validateOperationFeasibility)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_VALIDATE_FMC_AUTHORIZED_BUSINESS_MANAGER_PROFILED_SERVICE_VALIDATE_OPERATION_FEASIBILITY, serviceResponse, null, null);

        return serviceResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/financial-management-companies/{financial-management-company-id}/authorized-business-managers/{authorized-business-manager-id}/profiled-services/{profiled-service-id}/contracts")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_FMC_AUTHORIZED_BUSINESS_MANAGERS_PROFILED_SERVICES_CONTRACTS, logicalID = "listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts")
    public ServiceResponse<List<ServiceContract>> listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
            @DatoAuditable(omitir = true) @PathParam(FINANCIAL_MANAGEMENT_COMPANY_ID) final String financialManagementCompanyId,
            @DatoAuditable(omitir = true) @PathParam(AUTHORIZED_BUSINESS_MANAGER_ID) final String authorizedBusinessManagerId,
            @PathParam(PROFILED_SERVICE_ID) final String profiledServiceId,
            @QueryParam("paginationKey") final String paginationKey,
            @QueryParam("pageSize") final Integer pageSize) {
        LOG.info("----- Invoking service listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts -----");

        Map<String, Object> pathParams = new HashMap<>();
        pathParams.put(FINANCIAL_MANAGEMENT_COMPANY_ID, financialManagementCompanyId);
        pathParams.put(AUTHORIZED_BUSINESS_MANAGER_ID, authorizedBusinessManagerId);
        pathParams.put(PROFILED_SERVICE_ID, profiledServiceId);

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("paginationKey", paginationKey);
        queryParams.put("pageSize", pageSize);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_FMC_AUTHORIZED_BUSINESS_MANAGERS_PROFILED_SERVICES_CONTRACTS, null, pathParams, queryParams);

        DTOIntServiceContract dto = srvIntFinancialManagementCompanies.listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts(
                listFMCAuthorizedBusinessManagersProfiledServicesContractsMapper.mapIn(
                        pathParams.get(FINANCIAL_MANAGEMENT_COMPANY_ID).toString(),
                        pathParams.get(AUTHORIZED_BUSINESS_MANAGER_ID).toString(),
                        pathParams.get(PROFILED_SERVICE_ID).toString(),
                        (String) queryParams.get("paginationKey"),
                        (Integer) queryParams.get("pageSize")));

        if (dto == null) {
            return null;
        }

        Pagination pagination = null;
        if (dto.getPagination() != null) {
            pagination = GabiPaginationMapper.perform(businessServicesToolkitManager.getPaginationBuilder()
                    .setPagination(SrvFinancialManagementCompaniesV0.class, "listFinancialManagementCompaniesAuthorizedBusinessManagersProfiledServicesContracts", uriInfo,
                            dto.getPagination().getPaginationKey(), null,
                            dto.getPagination().getPageSize(), null,
                            null, null, null).build());
        }

        ServiceResponse<List<ServiceContract>> serviceResponse = listFMCAuthorizedBusinessManagersProfiledServicesContractsMapper.mapOut(dto.getData(), pagination);

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_FMC_AUTHORIZED_BUSINESS_MANAGERS_PROFILED_SERVICES_CONTRACTS, serviceResponse, null, null);

        return serviceResponse;
    }

    @Override
    @POST
    @Path("/financial-management-companies")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANY, logicalID = "financialManagementCompanies")
    public ServiceResponse<FinancialManagementCompanies> createFinancialManagementCompany(FinancialManagementCompanies financialManagementCompanies) {
        LOG.info("----- Invoking service createFinancialManagementCompany -----");
        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANY, financialManagementCompanies, null, null);
        ServiceResponse<FinancialManagementCompanies> serviceResponse = createFinancialManagementCompanyMapper.mapOut(
                srvIntFinancialManagementCompanies.createFinancialManagementCompany(createFinancialManagementCompanyMapper.mapIn(
                        financialManagementCompanies)));

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_CREATE_FINANCIAL_MANAGEMENT_COMPANY, serviceResponse, null, null);
        return serviceResponse;
    }
}
