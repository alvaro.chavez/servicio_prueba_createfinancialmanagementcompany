package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "business", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "business", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Business implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Business unique identifier.
     */
    private String id;
    /**
     * Business documents of a company.
     */
    @Valid
    private List<BusinessDocument> businessDocuments;
    /**
     *  Associated product to reference-customer.
     */
    private Product product;
    /**
     * Information about managed business.
     */
    @Valid
    private BusinessManagement businessManagement;
    /**
     * Limit amount for the chosen netcash product. This limit is the maximum
     * amount of money a user could use for any transaction.
     */
    @Valid
    private LimitAmount limitAmount;

    public List<BusinessDocument> getBusinessDocuments() {
        return businessDocuments;
    }

    public void setBusinessDocuments(List<BusinessDocument> businessDocuments) {
        this.businessDocuments = businessDocuments;
    }

    public BusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public void setBusinessManagement(BusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public LimitAmount getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(LimitAmount limitAmount) {
        this.limitAmount = limitAmount;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }
}
