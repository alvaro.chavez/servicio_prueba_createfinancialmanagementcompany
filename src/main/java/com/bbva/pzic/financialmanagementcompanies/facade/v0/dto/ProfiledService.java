package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "profiledService", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "profiledService", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProfiledService implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Profiled service identifier for the authorized business manager.
     */
    @DatoAuditable(omitir = true)
    private String id;
    /**
     * List of services.
     */
    private ServiceContract service;
    /**
     * Criteria that administrators will have when performing operations over
     * services. This configuration establishes the permissions for a certain
     * service. For all those services that are not configured here, they will
     * inherit the permissions configuration at the manager level.
     */
    private OperationRights operationRights;
    /**
     * Identify if the profiled services need a authorization of auditor to
     * perform a operation. This value it’s necessary to know if the profiled
     * service has auditor. if the value is TRUE the operation, additional to
     * the signatures of representants, will need the signature of auditor to be
     * completed.
     */
    private Boolean hasAssignedAuditor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServiceContract getService() {
        return service;
    }

    public void setService(ServiceContract service) {
        this.service = service;
    }

    public OperationRights getOperationRights() {
        return operationRights;
    }

    public void setOperationRights(OperationRights operationRights) {
        this.operationRights = operationRights;
    }

    public Boolean getHasAssignedAuditor() {
        return hasAssignedAuditor;
    }

    public void setHasAssignedAuditor(Boolean hasAssignedAuditor) {
        this.hasAssignedAuditor = hasAssignedAuditor;
    }
}
