package com.bbva.pzic.financialmanagementcompanies.facade.v0.dto;

import com.bbva.pzic.financialmanagementcompanies.business.dto.ValidationGroup;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "limitAmount", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlType(name = "limitAmount", namespace = "urn:com:bbva:pzic:financialmanagementcompanies:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class LimitAmount implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Maximum amount allowed.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    private BigDecimal amount;
    /**
     * String based on ISO-4217 for specifying the currency related to the
     * current amount.
     */
    @NotNull(groups = ValidationGroup.CreateSubscriptionRequest.class)
    private String currency;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
