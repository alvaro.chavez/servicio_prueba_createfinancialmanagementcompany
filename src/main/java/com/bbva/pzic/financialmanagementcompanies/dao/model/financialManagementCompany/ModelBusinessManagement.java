package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ManagementType;

public class ModelBusinessManagement {
    private ModelManagementType managementType;

    public ModelManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ModelManagementType managementType) {
        this.managementType = managementType;
    }
}
