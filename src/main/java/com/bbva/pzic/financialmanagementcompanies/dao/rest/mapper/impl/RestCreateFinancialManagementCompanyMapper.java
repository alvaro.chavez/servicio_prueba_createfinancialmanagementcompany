package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.*;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyData;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestCreateFinancialManagementCompanyMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RestCreateFinancialManagementCompanyMapper implements IRestCreateFinancialManagementCompanyMapper {

    private static final Log LOG = LogFactory.getLog(RestCreateFinancialManagementCompanyMapper.class);

    @Override
    public ModelFinancialManagementCompaniesRequest mapIn(DTOIntFinancialManagementCompanies dtoIn) {
        LOG.info("... called method ModelFinancialManagementCompaniesRequest.mapIn ...");
        if(dtoIn == null){
            return null;
        }
        ModelFinancialManagementCompaniesRequest financialManagementCompaniesRequest = new ModelFinancialManagementCompaniesRequest();
        financialManagementCompaniesRequest.setContract(mapInContract(dtoIn.getContract()));
        financialManagementCompaniesRequest.setProduct(mapInProduct(dtoIn.getProduct()));
        financialManagementCompaniesRequest.setBusiness(mapInBusiness(dtoIn.getBusiness()));
        financialManagementCompaniesRequest.setReviewers(mapInReviewers(dtoIn.getReviewers()));
        financialManagementCompaniesRequest.setNetCashType(mapInNetCashType(dtoIn.getNetcashType()));
        financialManagementCompaniesRequest.setRelationType(mapInRelationType(dtoIn.getRelationType()));

        return financialManagementCompaniesRequest;
    }

    private List<ModelReviewer> mapInReviewers(List<DTOIntReviewerNetcash> reviewers) {
        LOG.info("... called method ModelFinancialManagementCompaniesRequest.mapOut ...");
        if(CollectionUtils.isEmpty(reviewers)){
            return null;
        }
        return reviewers.stream().filter(Objects::nonNull).map(this::mapInReviewer).collect(Collectors.toList());
    }

    private ModelReviewer mapInReviewer(DTOIntReviewerNetcash dtoIntReviewerNetcash) {
        ModelReviewer dtoIn = new ModelReviewer();
        dtoIn.setProfessionPosition(dtoIntReviewerNetcash.getProfessionPosition());
        dtoIn.setRegistrationIdentifier(dtoIntReviewerNetcash.getRegistrationIdentifier());
        dtoIn.setBusinessAgentId(dtoIntReviewerNetcash.getBusinessAgentId());
        dtoIn.setUnitManagement(dtoIntReviewerNetcash.getUnitManagement());
        dtoIn.setReviewerType(mapInReviewerType(dtoIntReviewerNetcash.getReviewerType()));
        dtoIn.setBank(mapInBank(dtoIntReviewerNetcash.getBank()));
        dtoIn.setProfile(mapInProfile(dtoIntReviewerNetcash.getProfile()));
        dtoIn.setContractDetails(mapInContractDetails(dtoIntReviewerNetcash.getContactDetails()));
        return dtoIn;
    }

    private List<ModelContactDetail> mapInContractDetails(List<DTOIntContactDetail> contactDetails) {
        if(CollectionUtils.isEmpty(contactDetails))
            return null;
        return contactDetails.stream().filter(Objects::nonNull).map(this::mapInContractDetail).collect(Collectors.toList());
    }

    private ModelContactDetail mapInContractDetail(DTOIntContactDetail dtoIntContactDetail) {
        ModelContactDetail dtoIn = new ModelContactDetail();
        dtoIn.setContact(dtoIntContactDetail.getContact());
        dtoIn.setContactType(dtoIntContactDetail.getContactType());
        return dtoIn;
    }

    private ModelProfile mapInProfile(DTOIntProfile profile) {
        if(profile == null)
            return null;
        ModelProfile dtoIn = new ModelProfile();
        dtoIn.setId(profile.getId());
        return dtoIn;
    }

    private ModelBank mapInBank(DTOIntBank bank) {
        if(bank == null)
            return null;
        ModelBank dtoIn = new ModelBank();
        dtoIn.setId(bank.getId());
        dtoIn.setBranch(mapInBranch(bank.getBranch()));
        return  dtoIn;
    }

    private ModelBranch mapInBranch(DTOIntBranch branch) {
        if(branch == null)
            return null;
        ModelBranch dtoIn = new ModelBranch();
        dtoIn.setId(branch.getId());
        return dtoIn;
    }

    private ModelReviewerType mapInReviewerType(DTOIntReviewerType reviewerType) {
        if(reviewerType == null){
            return null;
        }
        ModelReviewerType dtoIn = new ModelReviewerType();
        dtoIn.setId(reviewerType.getId());
        return dtoIn;
    }

    private ModelRelationType mapInRelationType(DTOIntRelationType relationType) {
        if(relationType == null){
            return null;
        }
        ModelRelationType dtoIn = new ModelRelationType();
        dtoIn.setId(relationType.getId());
        return dtoIn;
    }

    private ModelNetCashType mapInNetCashType(DTOIntNetcashType netcashType) {
        if(netcashType == null){
            return null;
        }
        ModelNetCashType dtoIn = new ModelNetCashType();
        dtoIn.setId(netcashType.getId());
        dtoIn.setVersion(mapInVersion(netcashType.getVersion()));
        return dtoIn;
    }

    private ModelVersion mapInVersion(DTOIntVersionProduct version) {
        if(version == null){
            return null;
        }
        ModelVersion dtoIn = new ModelVersion();
        dtoIn.setId(version.getId());
        return dtoIn;
    }

    private ModelBusiness mapInBusiness(DTOIntBusiness business) {
        if(business == null){
            return null;
        }
        ModelBusiness dtoIn = new ModelBusiness();
        dtoIn.setLimitAmount(mapInLimitAmount(business.getLimitAmount()));
        dtoIn.setBusinessManagement(mapInBusinessManagement(business.getBusinessManagement()));
        dtoIn.setBusinessDocuments(mapInBusinessDocuments(business.getBusinessDocuments()));
        return dtoIn;
    }

    private List<ModelBusinessDocument> mapInBusinessDocuments(List<DTOIntBusinessDocument> businessDocuments) {
        if(CollectionUtils.isEmpty(businessDocuments)){
            return null;
        }
        return businessDocuments.stream().filter(Objects::nonNull).map(this::mapInBusinessDocument).collect(Collectors.toList());
    }

    private ModelBusinessDocument mapInBusinessDocument(DTOIntBusinessDocument dtoIntBusinessDocument) {
        ModelBusinessDocument dtoIn = new ModelBusinessDocument();
        dtoIn.setDocumentNumber(dtoIntBusinessDocument.getDocumentNumber());
        dtoIn.setExpirationDate(dtoIntBusinessDocument.getExpirationDate());
        dtoIn.setIssueDate(dtoIntBusinessDocument.getIssueDate());
        dtoIn.setBusinessDocumentType(mapInBusinessDocumentType(dtoIntBusinessDocument.getBusinessDocumentType()));
        return dtoIn;
    }

    private ModelBusinessDocumentType mapInBusinessDocumentType(DTOIntBusinessDocumentType businessDocumentType) {
        if(businessDocumentType == null){
            return null;
        }
        ModelBusinessDocumentType dtoIn = new ModelBusinessDocumentType();
        dtoIn.setId(businessDocumentType.getId());
        return dtoIn;
    }


    private ModelBusinessManagement mapInBusinessManagement(DTOIntBusinessManagement businessManagement) {
        if(businessManagement == null){
            return null;
        }
        ModelBusinessManagement dtoIn = new ModelBusinessManagement();
        dtoIn.setManagementType(mapInManagementType(businessManagement.getManagementType()));
        return dtoIn;
    }

    private ModelManagementType mapInManagementType(DTOIntManagementType managementType) {
        if(managementType == null){
            return null;
        }
        ModelManagementType dtoIn = new ModelManagementType();
        dtoIn.setId(managementType.getId());
        return dtoIn;
    }

    private ModelLimitAmount mapInLimitAmount(DTOIntLimitAmount limitAmount) {
        if(limitAmount == null){
            return null;
        }
        ModelLimitAmount dtoIn = new ModelLimitAmount();
        dtoIn.setAmount(limitAmount.getAmount());
        dtoIn.setCurrency(limitAmount.getCurrency());
        return dtoIn;
    }

    private ModelRelatedProduct mapInProduct(DTOIntRelatedProduct product) {
        if(product ==null){
            return null;
        }
        ModelRelatedProduct dtoIn = new ModelRelatedProduct();
        dtoIn.setId(product.getId());
        dtoIn.setProductType(mapInProductType(product.getProductType()));
        return dtoIn;
    }

    private ModelProductType mapInProductType(DTOIntProductType productType) {
        if(productType == null){
            return null;
        }
        ModelProductType dtoIn = new ModelProductType();
        dtoIn.setId(productType.getId());
        return dtoIn;
    }

    private ModelContract mapInContract(DTOIntContract contract) {
        if(contract == null){
            return null;
        }
        ModelContract modelContract = new ModelContract();
        modelContract.setId(contract.getId());
        return modelContract;
    }

    @Override
    public FinancialManagementCompanies mapOut(ModelFinancialManagementCompanyResponse dtoOut) {
        if(dtoOut == null || dtoOut.getData() == null)
            return null;
        FinancialManagementCompanies financialManagementCompanies = new FinancialManagementCompanies();
        financialManagementCompanies.setId(dtoOut.getData().getId());
        return financialManagementCompanies;
    }
}
