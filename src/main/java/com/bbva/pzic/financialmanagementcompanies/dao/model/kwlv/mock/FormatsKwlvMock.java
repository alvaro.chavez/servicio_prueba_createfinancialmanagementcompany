package com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.mock;

import com.bbva.pzic.financialmanagementcompanies.dao.model.kwlv.FormatoKNECLVS0;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public final class FormatsKwlvMock {

    private static final FormatsKwlvMock INSTANCE = new FormatsKwlvMock();
    private ObjectMapperHelper objectMapperHelper = ObjectMapperHelper.getInstance();

    private FormatsKwlvMock() {
    }

    public static FormatsKwlvMock getInstance() {
        return INSTANCE;
    }

    public FormatoKNECLVS0 getFormatoKNECLVS0() throws IOException {
        return objectMapperHelper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                "com/bbva/pzic/financialmanagementcompanies/dao/model/kwlv/mock/formatoKNECLVS0.json"), FormatoKNECLVS0.class);
    }

    public FormatoKNECLVS0 getFormatoKNECLVS0Empty() {
        return new FormatoKNECLVS0();
    }
}
