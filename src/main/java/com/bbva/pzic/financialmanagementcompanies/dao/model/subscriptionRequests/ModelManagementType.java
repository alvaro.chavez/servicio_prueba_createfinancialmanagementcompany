package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelManagementType {

    private String id;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}