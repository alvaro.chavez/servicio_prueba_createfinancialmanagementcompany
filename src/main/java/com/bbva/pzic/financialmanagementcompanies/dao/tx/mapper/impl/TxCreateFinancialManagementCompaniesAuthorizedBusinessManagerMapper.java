package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBEHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBSHE;
import com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper.ITxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
@Mapper
public class TxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper extends ConfigurableMapper implements ITxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper {

    @Autowired
    private EnumMapper enumMapper;

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);
        factory.classMap(InputCreateAuthorizedBusinessManager.class, FormatoKNECBEHE.class)
                .field("financialManagementCompanyId", "codemp")
                .field("businessManagerId", "usuid")
                .field("validationRightsId", "podval")
                .field("permissionTypeId", "tipper")
                .field("businessManagementStatus", "estger")
                .register();

        factory.classMap(FormatoKNECBSHE.class, CreateAuthorizedBusinessManager.class)
                .field("usuids", "id")
                .field("usuids", "businessManager.id")
                .field("podvals", "businessManagement.operationsRights.signature.validationRights.id")
                .field("tippers", "businessManagement.operationsRights.permissionType.id")
                .field("podvals", "businessManagement.usersAdministrationRights.id")
                .field("gerneg", "businessManagement.status")

                .register();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FormatoKNECBEHE mapIn(final InputCreateAuthorizedBusinessManager dtoIn) {
        return map(dtoIn, FormatoKNECBEHE.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CreateAuthorizedBusinessManager mapOut(final FormatoKNECBSHE formatOutput) {
        CreateAuthorizedBusinessManager createAuthorizedBusinessManager = map(formatOutput, CreateAuthorizedBusinessManager.class);
        if (formatOutput.getPodvals() != null) {
            createAuthorizedBusinessManager.getBusinessManagement().getOperationsRights().getSignature().getValidationRights()
                    .setId(enumMapper.getEnumValue("authorizedBusinessManager.signature.id", formatOutput.getPodvals()));
        }
        if (formatOutput.getTippers() != null) {
            createAuthorizedBusinessManager.getBusinessManagement().getOperationsRights().getPermissionType()
                    .setId(enumMapper.getEnumValue("authorizedBusinessManager.operationsRights.permissionType.id", formatOutput.getTippers()));
        }
        if (formatOutput.getPodvals() != null) {
            createAuthorizedBusinessManager.getBusinessManagement().getUsersAdministrationRights()
                    .setId(enumMapper.getEnumValue("authorizedBusinessManager.signature.id", formatOutput.getPodvals()));
        }

        if (formatOutput.getGerneg() != null) {
            createAuthorizedBusinessManager.getBusinessManagement()
                    .setStatus(enumMapper.getEnumValue("authorizedBusinessManager.status", formatOutput.getGerneg()));
        }

        return createAuthorizedBusinessManager;
    }
}
