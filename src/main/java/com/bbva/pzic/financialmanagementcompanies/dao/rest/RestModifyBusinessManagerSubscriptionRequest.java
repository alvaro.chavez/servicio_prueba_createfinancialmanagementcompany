package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestModifyBusinessManagerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPutConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
@Component
public class RestModifyBusinessManagerSubscriptionRequest extends RestPutConnection<ModelSubscriptionRequest, Object> {

    private static final String MODIFY_BUSINESS_MANAGER_SUBSCRIPTION_REQUEST_URL = "servicing.url.financialManagementCompanies.modifyBusinessManagerSubscriptionRequest";
    private static final String MODIFY_BUSINESS_MANAGER_SUBSCRIPTION_REQUEST_USE_PROXY = "servicing.proxy.financialManagementCompanies.modifyBusinessManagerSubscriptionRequest";

    @Autowired
    private IRestModifyBusinessManagerSubscriptionRequestMapper mapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(MODIFY_BUSINESS_MANAGER_SUBSCRIPTION_REQUEST_USE_PROXY, false);
    }

    public void invoke(final InputModifyBusinessManagerSubscriptionRequest input) {
        connect(MODIFY_BUSINESS_MANAGER_SUBSCRIPTION_REQUEST_URL, mapper.mapInPathParams(input), mapper.mapIn(input));
    }

    @Override
    protected void evaluateResponse(Object response, int statusCode) {
    }
}