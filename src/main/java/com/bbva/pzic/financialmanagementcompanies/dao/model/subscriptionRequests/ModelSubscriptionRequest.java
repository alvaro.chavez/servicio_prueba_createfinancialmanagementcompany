package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

/**
 * @author Entelgy
 */
public class ModelSubscriptionRequest {

    @JsonProperty("subscription-request-id")
    private String subscriptionRequestId;
    private ModelBusinessSubscriptionRequest business;
    private ModelLimitAmount limitAmount;
    private List<ModelRelatedContract> relatedContracts;
    private ModelProductSubscriptionRequest product;
    private ModelJoint joint;
    private ModelBusinessManagerSubscriptionRequest businessManager;
    private List<ModelBusinessManagerSubscriptionRequest> businessManagers;
    private String unitManagement;
    private ModelBranch branch;
    private String id;
    private ModelStatusSubscriptionRequest status;
    private String comment;

    private Date openingDate;
    private List<ModelParticipant> participants;
    private ModelForm form;

    public String getSubscriptionRequestId() {
        return subscriptionRequestId;
    }

    public void setSubscriptionRequestId(String subscriptionRequestId) {
        this.subscriptionRequestId = subscriptionRequestId;
    }

    public ModelBusinessSubscriptionRequest getBusiness() {
        return business;
    }

    public void setBusiness(ModelBusinessSubscriptionRequest business) {
        this.business = business;
    }

    public ModelLimitAmount getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(ModelLimitAmount limitAmount) {
        this.limitAmount = limitAmount;
    }

    public List<ModelRelatedContract> getRelatedContracts() {
        return relatedContracts;
    }

    public void setRelatedContracts(List<ModelRelatedContract> relatedContracts) {
        this.relatedContracts = relatedContracts;
    }

    public ModelProductSubscriptionRequest getProduct() {
        return product;
    }

    public void setProduct(ModelProductSubscriptionRequest product) {
        this.product = product;
    }

    public ModelJoint getJoint() {
        return joint;
    }

    public void setJoint(ModelJoint joint) {
        this.joint = joint;
    }

    public ModelBusinessManagerSubscriptionRequest getBusinessManager() {
        return businessManager;
    }

    public void setBusinessManager(ModelBusinessManagerSubscriptionRequest businessManager) {
        this.businessManager = businessManager;
    }

    public List<ModelBusinessManagerSubscriptionRequest> getBusinessManagers() {
        return businessManagers;
    }

    public void setBusinessManagers(List<ModelBusinessManagerSubscriptionRequest> businessManagers) {
        this.businessManagers = businessManagers;
    }

    public String getUnitManagement() {
        return unitManagement;
    }

    public void setUnitManagement(String unitManagement) {
        this.unitManagement = unitManagement;
    }

    public ModelBranch getBranch() {
        return branch;
    }

    public void setBranch(ModelBranch branch) {
        this.branch = branch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelStatusSubscriptionRequest getStatus() {
        return status;
    }

    public void setStatus(ModelStatusSubscriptionRequest status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public List<ModelParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ModelParticipant> participants) {
        this.participants = participants;
    }

    public ModelForm getForm() {
        return form;
    }

    public void setForm(ModelForm form) {
        this.form = form;
    }
}