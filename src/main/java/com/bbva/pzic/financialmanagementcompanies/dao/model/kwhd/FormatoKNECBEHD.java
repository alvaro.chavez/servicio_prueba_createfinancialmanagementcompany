package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>KNECBEHD</code> de la transacci&oacute;n <code>KWHD</code>
 *
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "KNECBEHD")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoKNECBEHD {

	/**
	 * <p>Campo <code>TIPUS1</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "TIPUS1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipus1;

	/**
	 * <p>Campo <code>TIPUS2</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "TIPUS2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipus2;

	/**
	 * <p>Campo <code>TIPUS3</code>, &iacute;ndice: <code>3</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 3, nombre = "TIPUS3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipus3;

	/**
	 * <p>Campo <code>CODEMP</code>, &iacute;ndice: <code>4</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 4, nombre = "CODEMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codemp;

	/**
	 * <p>Campo <code>NOMUSU</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "NOMUSU", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String nomusu;

	/**
	 * <p>Campo <code>DOCID</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "DOCID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String docid;

	/**
	 * <p>Campo <code>NRODID</code>, &iacute;ndice: <code>7</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 7, nombre = "NRODID", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String nrodid;

	/**
	 * <p>Campo <code>TIPCO0</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "TIPCO0", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco0;

	/**
	 * <p>Campo <code>DACO0</code>, &iacute;ndice: <code>9</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 9, nombre = "DACO0", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco0;

	/**
	 * <p>Campo <code>TIPCO1</code>, &iacute;ndice: <code>10</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 10, nombre = "TIPCO1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco1;

	/**
	 * <p>Campo <code>DACO1</code>, &iacute;ndice: <code>11</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 11, nombre = "DACO1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco1;

	/**
	 * <p>Campo <code>TIPCO2</code>, &iacute;ndice: <code>12</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 12, nombre = "TIPCO2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco2;

	/**
	 * <p>Campo <code>DACO2</code>, &iacute;ndice: <code>13</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 13, nombre = "DACO2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco2;

	/**
	 * <p>Campo <code>TIPCO3</code>, &iacute;ndice: <code>14</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 14, nombre = "TIPCO3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String tipco3;

	/**
	 * <p>Campo <code>DACO3</code>, &iacute;ndice: <code>15</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 15, nombre = "DACO3", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 78, longitudMaxima = 78)
	private String daco3;
}