package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

import java.util.List;

public class ModelFinancialManagementCompaniesRequest {
    private ModelBusiness business;
    private ModelNetCashType netCashType;
    private ModelContract contract;
    private ModelRelatedProduct product;
    private ModelRelationType relationType;
    private List<ModelReviewer> reviewers;

    public ModelBusiness getBusiness() {
        return business;
    }

    public void setBusiness(ModelBusiness business) {
        this.business = business;
    }

    public ModelNetCashType getNetCashType() {
        return netCashType;
    }

    public void setNetCashType(ModelNetCashType netCashType) {
        this.netCashType = netCashType;
    }

    public ModelContract getContract() {
        return contract;
    }

    public void setContract(ModelContract contract) {
        this.contract = contract;
    }

    public ModelRelatedProduct getProduct() {
        return product;
    }

    public void setProduct(ModelRelatedProduct product) {
        this.product = product;
    }

    public ModelRelationType getRelationType() {
        return relationType;
    }

    public void setRelationType(ModelRelationType relationType) {
        this.relationType = relationType;
    }

    public List<ModelReviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<ModelReviewer> reviewers) {
        this.reviewers = reviewers;
    }
}
