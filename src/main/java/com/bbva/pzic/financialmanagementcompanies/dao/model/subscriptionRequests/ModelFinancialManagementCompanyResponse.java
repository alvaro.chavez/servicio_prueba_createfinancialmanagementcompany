package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelFinancialManagementCompanyResponse {

    private ModelFinancialManagementCompanyData data;
    private List<Message> messages;

    public ModelFinancialManagementCompanyData getData() {
        return data;
    }

    public void setData(ModelFinancialManagementCompanyData data) {
        this.data = data;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}