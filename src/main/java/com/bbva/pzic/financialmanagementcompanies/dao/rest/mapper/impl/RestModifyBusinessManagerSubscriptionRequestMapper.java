package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputModifyBusinessManagerSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.BackendParamsNames;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelSubscriptionRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestModifyBusinessManagerSubscriptionRequestMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import com.bbva.pzic.financialmanagementcompanies.util.orika.MapperFactory;
import com.bbva.pzic.financialmanagementcompanies.util.orika.impl.ConfigurableMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 14/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class RestModifyBusinessManagerSubscriptionRequestMapper extends ConfigurableMapper implements IRestModifyBusinessManagerSubscriptionRequestMapper {

    private static final Log LOG = LogFactory.getLog(RestListSubscriptionRequestsMapper.class);

    @Override
    protected void configure(MapperFactory factory) {
        super.configure(factory);

        factory.classMap(InputModifyBusinessManagerSubscriptionRequest.class, ModelSubscriptionRequest.class)
                .field("businessManagerSubscriptionRequest.targetUserId", "businessManager.userIdHost")
                .register();
    }

    @Override
    public Map<String, String> mapInPathParams(final InputModifyBusinessManagerSubscriptionRequest input) {
        LOG.info("... called method RestModifyBusinessManagerSubscriptionRequestMapper.mapInPathParams ...");
        Map<String, String> pathParams = new HashMap<>();
        pathParams.put(BackendParamsNames.SUBSCRIPTION_REQUEST_ID, input.getSubscriptionRequestId());
        pathParams.put(BackendParamsNames.BUSINESS_MANAGER_ID, input.getBusinessManagerId());
        return pathParams;
    }

    @Override
    public ModelSubscriptionRequest mapIn(InputModifyBusinessManagerSubscriptionRequest input) {
        LOG.info("... called method RestModifyBusinessManagerSubscriptionRequestMapper.mapIn ...");
        return map(input, ModelSubscriptionRequest.class);
    }
}