package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.mock;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBEHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.PeticionTransaccionKwhe;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.RespuestaTransaccionKwhe;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invocador de la transacci&oacute;n <code>KWHD</code>
 *
 * @see PeticionTransaccionKwhe
 * @see RespuestaTransaccionKwhe
 */
@Component("transaccionKwhe")
public class TransaccionKwheMock implements InvocadorTransaccion<PeticionTransaccionKwhe, RespuestaTransaccionKwhe> {

    public static final String TEST_NO_RESULT = "99999999";

    @Override
    public RespuestaTransaccionKwhe invocar(PeticionTransaccionKwhe transaccion) {
        RespuestaTransaccionKwhe response = new RespuestaTransaccionKwhe();
        response.setCodigoRetorno("OK_COMMIT");
        response.setCodigoControl("OK");

        FormatoKNECBEHE format = transaccion.getCuerpo().getParte(FormatoKNECBEHE.class);

        if (TEST_NO_RESULT.equals(format.getCodemp())) {
            return response;
        }

        try {
            CopySalida copy = new CopySalida();
            copy.setCopy(FormatsKwheMock.getInstance().getFormatoKNECBSHE());
            response.getCuerpo().getPartes().add(copy);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return response;
    }

    @Override
    public RespuestaTransaccionKwhe invocarCache(PeticionTransaccionKwhe transaccion) {
        return null;
    }

    @Override
    public void vaciarCache() {
    }
}
