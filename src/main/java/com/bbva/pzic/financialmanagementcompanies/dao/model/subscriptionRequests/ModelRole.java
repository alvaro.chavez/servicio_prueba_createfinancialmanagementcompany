package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

/**
 * @author Entelgy
 */
public class ModelRole {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}