package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

public class ModelNetCashType {

    private String id;
    private ModelVersion version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelVersion getVersion() {
        return version;
    }

    public void setVersion(ModelVersion version) {
        this.version = version;
    }
}
