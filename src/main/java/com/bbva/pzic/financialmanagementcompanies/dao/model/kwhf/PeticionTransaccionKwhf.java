package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhf;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;
import org.springframework.roo.addon.javabean.annotations.RooToString;

/**
 * <p>Transacci&oacute;n <code>KWHF</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionKwhf</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionKwhf</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECBEHF.D1181119.txt
 * KNECBEHF�ENTRADA CONS. USUARIOS ASO    �F�01�00024�01�00001�USUID  �USUID               �A�024�0�R�        �
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.KWHF.D1181119.txt
 * KWHFKNECBSHFKNECBSHFKN2CKWHF1S0155S000                     P016788 2018-03-07-11.16.12.469175P016788 2018-03-07-11.16.12.469221
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTCCT.KWHF.D1181119.txt
 * KWHFCONS. USUARIOS NETCASH ONBOARDING  KN        KN2CKWHFPBDKNPO KNECBEHF            KWHF  NS0500NNNNNN    SSTN    C   SNNSSNNN  NN                2018-03-06P016788 2018-09-2710.47.38P016788 2018-03-06-18.04.02.049638P016788 0001-01-010001-01-01
 *
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.KNECBSHF.D1181119.txt
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�01�00001�NOMEMP �NOMBRE DE EMPRESA   �A�060�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�02�00061�USUIDS �USUIDS              �A�024�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�03�00085�NOMUSU �NOMUSU              �A�030�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�04�00115�DOCID  �DOCID               �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�05�00116�DESCID �DESCID              �A�012�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�06�00128�TIPUS1 �TIPUS1              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�07�00129�TIPUS2 �TIPUS2              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�08�00130�TIPUS3 �TIPUS3              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�09�00131�TIPCO0 �TIPCO0              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�10�00132�DACO0  �DACO0               �A�078�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�11�00210�TIPCO1 �TIPCO1              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�12�00211�DACO1  �DACO1               �A�078�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�13�00289�TIPCO2 �TIPCO2              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�14�00290�DACO2  �DACO2               �A�078�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�15�00368�TIPCO3 �TIPCO3              �A�001�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�16�00369�DACO3  �DACO3               �A�078�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�17�00447�FECALT �FECALT              �A�010�0�S�        �
 * KNECBSHF�SALIDA CONS.USUARIOS  ASO     �X�18�00464�18�00457�HORALT �HORALT              �A�008�0�S�        �
 *
</pre></code>
 *
 * @see RespuestaTransaccionKwhf
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "KWHF",
	tipo = 1,
	subtipo = 1,
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionKwhf.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoKNECBEHF.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionKwhf implements MensajeMultiparte {

	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();

	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}

}