package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

public class ModelBank {
    private String id;
    private ModelBranch branch;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelBranch getBranch() {
        return branch;
    }

    public void setBranch(ModelBranch branch) {
        this.branch = branch;
    }
}
