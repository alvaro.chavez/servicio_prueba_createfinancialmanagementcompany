package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;

import java.util.List;

/**
 * @author Entelgy
 */
public class ModelSubscriptionResponse {

    private String id;
    private List<Message> messages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}