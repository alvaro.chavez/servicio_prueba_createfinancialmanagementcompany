package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

public class ModelContactDetail {
    private String contact;
    private String contactType;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }
}
