package com.bbva.pzic.financialmanagementcompanies.dao.rest;

import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.ErrorSeverity;
import com.bbva.jee.arq.spring.core.servicing.gce.xml.instance.Message;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ModelSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ResponseSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestSendMailMapper;
import com.bbva.pzic.financialmanagementcompanies.util.connection.rest.RestPostConnection;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;

/**
 * Created on 26/09/2018.
 *
 * @author Entelgy
 */
@Component
public class RestSendMail extends RestPostConnection<ModelSendMail, ResponseSendMail> {

    private static final String URL = "servicing.url.financialManagementCompanies.sendEmailOtpBusinessManager.sendMail";
    private static final String USE_PROXY = "servicing.proxy.financialManagementCompanies.sendEmailOtpBusinessManager.sendMail";

    @Resource(name = "restSendMailMapper")
    private IRestSendMailMapper sendMailMapper;

    @PostConstruct
    public void init() {
        useProxy = configurationManager.getBooleanProperty(USE_PROXY, false);
    }

    public void invoke(InputSendEmailOtpFinancialManagementCompaniesBusinessManager entityPayload) {
        connect(URL, sendMailMapper.mapIn(entityPayload));
    }

    @Override
    protected void evaluateResponse(ResponseSendMail response, int statusCode) {
        evaluateMessagesResponse(Collections.singletonList(buildMessage(response)), "SMCPE1810337", statusCode);
    }

    private Message buildMessage(final ResponseSendMail response) {
        Message message = new Message();
        message.setCode(response.getCode());
        message.setMessage(response.getMessage());
        message.setType(ErrorSeverity.ERROR);
        return message;
    }
}
