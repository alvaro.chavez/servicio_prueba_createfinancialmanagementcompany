package com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests;

import java.util.List;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public class ModelFinancialManagementCompanies {

    private ModelBusiness business;
    private ModelNetcashType netcashType;
    private List<ModelReviewerNetcash> reviewers;

    public ModelBusiness getBusiness() {
        return business;
    }

    public void setBusiness(ModelBusiness business) {
        this.business = business;
    }

    public ModelNetcashType getNetcashType() {
        return netcashType;
    }

    public void setNetcashType(ModelNetcashType netcashType) {
        this.netcashType = netcashType;
    }

    public List<ModelReviewerNetcash> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<ModelReviewerNetcash> reviewers) {
        this.reviewers = reviewers;
    }
}