package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputCreateAuthorizedBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBEHE;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwhe.FormatoKNECBSHE;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.CreateAuthorizedBusinessManager;

/**
 * Created on 18/09/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateFinancialManagementCompaniesAuthorizedBusinessManagerMapper {

    FormatoKNECBEHE mapIn(InputCreateAuthorizedBusinessManager dtoIn);

    CreateAuthorizedBusinessManager mapOut(FormatoKNECBSHE formatOutput);
}
