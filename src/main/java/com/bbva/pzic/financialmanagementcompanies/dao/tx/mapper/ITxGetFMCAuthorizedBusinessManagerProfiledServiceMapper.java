package com.bbva.pzic.financialmanagementcompanies.dao.tx.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.InputGetFMCAuthorizedBusinessManagerProfiledService;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPE0;
import com.bbva.pzic.financialmanagementcompanies.dao.model.kwfp.FormatoKNECFPS0;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.ProfiledService;

/**
 * Created on 04/03/2020.
 *
 * @author Entelgy
 */
public interface ITxGetFMCAuthorizedBusinessManagerProfiledServiceMapper {

    FormatoKNECFPE0 mapIn(InputGetFMCAuthorizedBusinessManagerProfiledService dtoIn);

    ProfiledService mapOut(FormatoKNECFPS0 formatOutput);
}
