package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

public class ModelBranch {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
