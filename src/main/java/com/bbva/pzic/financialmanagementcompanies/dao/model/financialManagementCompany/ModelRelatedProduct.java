package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

public class ModelRelatedProduct {
    private String id;
    private ModelProductType productType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ModelProductType getProductType() {
        return productType;
    }

    public void setProductType(ModelProductType productType) {
        this.productType = productType;
    }
}
