package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper;

import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntFinancialManagementCompanies;
import com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany.ModelFinancialManagementCompaniesRequest;
import com.bbva.pzic.financialmanagementcompanies.dao.model.subscriptionRequests.ModelFinancialManagementCompanyResponse;
import com.bbva.pzic.financialmanagementcompanies.facade.v0.dto.FinancialManagementCompanies;

public interface IRestCreateFinancialManagementCompanyMapper {
    ModelFinancialManagementCompaniesRequest mapIn(DTOIntFinancialManagementCompanies dtoIn);

    FinancialManagementCompanies mapOut(ModelFinancialManagementCompanyResponse dtoOut);
}
