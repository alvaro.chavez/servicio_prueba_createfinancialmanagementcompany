package com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.impl;

import com.bbva.jee.arq.spring.core.servicing.context.BackendContext;
import com.bbva.jee.arq.spring.core.servicing.context.ServiceInvocationContext;
import com.bbva.pzic.financialmanagementcompanies.business.dto.DTOIntEmail;
import com.bbva.pzic.financialmanagementcompanies.business.dto.InputSendEmailOtpFinancialManagementCompaniesBusinessManager;
import com.bbva.pzic.financialmanagementcompanies.dao.model.sendMail.ModelSendMail;
import com.bbva.pzic.financialmanagementcompanies.dao.rest.mapper.IRestSendMailMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.EnumMapper;
import com.bbva.pzic.financialmanagementcompanies.util.mappers.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Mapper
public class RestSendMailMapper implements IRestSendMailMapper {

    static final String EMAIL_SUBJECT = "smc.configuration.SMCPE1810337.email.subject";
    static final String EMAIL_SENDER = "smc.configuration.SMCPE1810337.email.to";
    private static final String WORK_EMAIL = "WORK_EMAIL";
    private static final String CONSTANTE_CODIGO_PLT = "PLT00530";
    @Autowired
    private ServiceInvocationContext serviceInvocationContext;

    @Autowired
    private EnumMapper enumMapper;

    @Override
    public ModelSendMail mapIn(InputSendEmailOtpFinancialManagementCompaniesBusinessManager entityPayload) {
        String aap = serviceInvocationContext.getProperty(BackendContext.AAP);
        String subject = enumMapper.getPropertyValueApp(aap, EMAIL_SUBJECT);
        String sender = enumMapper.getPropertyValueApp(aap, EMAIL_SENDER);

        ModelSendMail bodyDataRest = new ModelSendMail();
        bodyDataRest.setId(entityPayload.getBusinessManagerId());
        bodyDataRest.setSubject(subject);
        bodyDataRest.setSender(sender);
        bodyDataRest.setRecipients(new ArrayList<String>());
        bodyDataRest.setMessageBody(new ArrayList<String>());

        if (entityPayload.getEmailList() != null) {
            String sCorreo = sCorreo(entityPayload.getEmailList());
            bodyDataRest.getRecipients().add(sCorreo);
            bodyDataRest.getMessageBody().add("001".concat(entityPayload.getUserName()));
            bodyDataRest.getMessageBody().add("002".concat(rightSubstringUser(entityPayload.getBusinessManagerId())));
            bodyDataRest.getMessageBody().add("003".concat(entityPayload.getPassword()));
            bodyDataRest.getMessageBody().add("004".concat(getDate()));
            bodyDataRest.getMessageBody().add("005".concat(getHour()));
            bodyDataRest.getMessageBody().add("006".concat(entityPayload.getBusinessName()));
        }
        bodyDataRest.setCodeTemplate(CONSTANTE_CODIGO_PLT);
        return bodyDataRest;
    }

    private String sCorreo(List<DTOIntEmail> email) {
        String result = "";
        for (DTOIntEmail item : email) {
            if (item.getType().getId().equals(WORK_EMAIL)) {
                result = item.getValue();
                break;
            }
        }
        return result;
    }

    private String rightSubstringUser(String value) {
        if (value.length() < 8)
            return value;
        return value.substring(value.length() - 8);
    }

    private String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return formatter.format(date);
    }

    private String getHour() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
    }
}
