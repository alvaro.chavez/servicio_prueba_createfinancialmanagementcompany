package com.bbva.pzic.financialmanagementcompanies.dao.model.kwhd;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Invocador de la transacci&oacute;n <code>KWHD</code>
 *
 * @see PeticionTransaccionKwhd
 * @see RespuestaTransaccionKwhd
 */
@Component("transaccionKwhd")
public class TransaccionKwhd implements InvocadorTransaccion<PeticionTransaccionKwhd, RespuestaTransaccionKwhd> {

    @Autowired
    private ServicioTransacciones servicioTransacciones;

    @Override
    public RespuestaTransaccionKwhd invocar(PeticionTransaccionKwhd transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwhd.class, RespuestaTransaccionKwhd.class, transaccion);
    }

    @Override
    public RespuestaTransaccionKwhd invocarCache(PeticionTransaccionKwhd transaccion) throws ExcepcionTransaccion {
        return servicioTransacciones.invocar(PeticionTransaccionKwhd.class, RespuestaTransaccionKwhd.class, transaccion);
    }

    @Override
    public void vaciarCache() {
    }
}