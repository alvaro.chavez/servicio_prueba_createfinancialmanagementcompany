package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

import java.util.Date;

public class ModelBusinessDocument {

    private ModelBusinessDocumentType businessDocumentType;
    private Date issueDate;
    private Date expirationDate;
    private String documentNumber;

    public ModelBusinessDocumentType getBusinessDocumentType() {
        return businessDocumentType;
    }

    public void setBusinessDocumentType(ModelBusinessDocumentType businessDocumentType) {
        this.businessDocumentType = businessDocumentType;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
}
