package com.bbva.pzic.financialmanagementcompanies.dao.model.financialManagementCompany;

import java.util.Date;
import java.util.List;

public class ModelBusiness {
    private List<ModelBusinessDocument> businessDocuments;
    private ModelBusinessManagement businessManagement;
    private ModelLimitAmount limitAmount;

    public List<ModelBusinessDocument> getBusinessDocuments() {
        return businessDocuments;
    }

    public void setBusinessDocuments(List<ModelBusinessDocument> businessDocuments) {
        this.businessDocuments = businessDocuments;
    }

    public ModelBusinessManagement getBusinessManagement() {
        return businessManagement;
    }

    public void setBusinessManagement(ModelBusinessManagement businessManagement) {
        this.businessManagement = businessManagement;
    }

    public ModelLimitAmount getLimitAmount() {
        return limitAmount;
    }

    public void setLimitAmount(ModelLimitAmount limitAmount) {
        this.limitAmount = limitAmount;
    }
}
